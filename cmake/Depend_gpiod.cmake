include(Tools)

option(LIBGPIOD "Enable interaction with linux GPIO devices" ON)

if (LIBGPIOD)
  find_library(GPIOD_LIBRARY "gpiod")
  assert(GPIOD_LIBRARY MESSAGE "gpiod library not found")
endif(LIBGPIOD)
