include(Tools)

find_library(PTHREAD_LIBRARY "pthread")
assert(PTHREAD_LIBRARY MESSAGE "pthread library not found")
