
try_compile(HAS_STDATOMIC "${CMAKE_BINARY_DIR}" "${CMAKE_CURRENT_LIST_DIR}/has_stdatomic.c")
try_compile(HAS_GCCATOMIC "${CMAKE_BINARY_DIR}" "${CMAKE_CURRENT_LIST_DIR}/has_gcc_atomic.c")

if(NOT HAS_STDATOMIC)
    if(NOT HAS_GCCATOMIC)
        message(SEND_ERROR "No atomics type support")
    else()
        message(WARNING "No stdatomic found, using gcc built-in fallback")
    endif()
endif()
