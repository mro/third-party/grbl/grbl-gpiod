
#ifdef __STDC_NO_ATOMICS__
#error "Real-time thread implementation needs C11 atomics"
#endif
#include <stdatomic.h>

int main()
{
    atomic_int value;
    atomic_init(&value, 0);
    atomic_store(&value, 42);
    return atomic_load(&value);
}
