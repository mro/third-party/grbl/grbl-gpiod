# Arguments: SRCDIRS (list of directories to analyse)
separate_arguments(SRCDIRS)
separate_arguments(PROJECT_INCDIRS)
list(TRANSFORM PROJECT_INCDIRS PREPEND "-I")


set(S "${CMAKE_CURRENT_LIST_DIR}/..")

find_program(CLANG_TIDY_EXE NAMES clang-tidy)

foreach(F IN LISTS SRCDIRS)
    list(APPEND GLOB_PATTERN "${S}/${F}/*.c" "${S}/${F}/*.h")
endforeach()
file(GLOB_RECURSE SRCS RELATIVE "${S}" ${GLOB_PATTERN})

function(gen SRCS INCDIRS)
    foreach(F IN LISTS SRCS)
        message(STATUS "Running Clang Tidy on \"${F}\"")
        execute_process(
            COMMAND "${CLANG_TIDY_EXE}" "--quiet" "--fix" "${F}" "--" ${PROJECT_INCDIRS}
            WORKING_DIRECTORY "${S}"
            ERROR_STRIP_TRAILING_WHITESPACE)
    endforeach()
endfunction()

gen("${SRCS}" "${PROJECT_INCDIRS}")
