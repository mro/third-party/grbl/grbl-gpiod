
int main()
{
    int i;
    __atomic_load_n(&i, __ATOMIC_SEQ_CST);
    __atomic_store_n(&i, 12, __ATOMIC_SEQ_CST);
    return 0;
}
