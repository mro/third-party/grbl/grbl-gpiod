/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-03-14
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#ifndef GPIO_H
#define GPIO_H

#include <gpiod.h>
#include <stdint.h>

#include "grbl/nuts_bolts.h"

// number of GPIO-pin clusters required for controlling stepper motors
#define N_PIN_CLUSTER 3 // They are listed below:

#define STEP_PULSE 0  // cluster for the generetion of step pulses on axes (out)
#define STEP_DIR 1    // cluster for setting axes direction (out)
#define STEP_ENABLE 2 // cluster to enable/disable stepper motors power (out)

struct gpio_chip_t
{
  struct gpiod_line_bulk *bulk;
  int *values; // values to read or set on bulk lines
  struct gpio_chip_t *next;
};

/**
 * Cluster of pins meant for outputting binary signal, reading binary input
 * or monitoring event on GPIO lines. A single cluster can handle up to 8 lines.
 */
typedef struct
{
  uint8_t state; //!< byte representing the current logical level on the lines
  struct gpio_chip_t *chip_list;    //!< singly linked list of GPIO chip used
  struct gpiod_line *lines[N_AXIS]; //!< ordered array of GPIO lines handled by
                                    //!< the cluster
  int *values[N_AXIS]; //!< pointers to the corresponding GPIO chip values
} gpio_cluster_t;

extern gpio_cluster_t gpio[N_PIN_CLUSTER];

/**
 * @brief set-up GPIO lines according to the configuration
 * @param config path to the configuration file
 * @param err pointer to the location where the eventual error message is
 *            copied, if not NULL
 **/
bool gpio_setup(const char *config, char **err);

/**
 * @brief release all resources allocated
 **/
void gpio_release();

/**
 * @brief set values to a GPIO line cluster
 * @param cluster pointer to GPIO line cluster
 * @param pins values to be set
 * @param err pointer to the location where the eventual error message is
 *            copied, if not NULL
 **/
void gpio_set(gpio_cluster_t *cluster, uint8_t pins, char **err);

/**
 * @brief read values from a GPIO line cluster
 * @param cluster pointer to GPIO line cluster
 * @param err pointer to the location where the eventual error message is
 *            copied, if not NULL
 **/
uint8_t gpio_get(gpio_cluster_t *cluster, char **err);

#endif /* GPIO_H */
