/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-21
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#include "rt_thread.h"

#include <errno.h>
#include <pthread.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "atomic.h"
#include "utils.h"

#define NS_PER_SEC 1000000000
#define JITTER_CALIB_CYCLES 1000

#define RESUME_SIGNAL SIGUSR1

struct rt_thread
{
  atomic_long interval;
  long max_jitter;
  rt_thread_function_t routine;
  pthread_t id;
  sigset_t sig_set;
  atomic_bool started;
  atomic_bool paused;
  atomic_bool accurate_sleep;
};

/**
 * @brief add a nanoseconds interval to timespec time
 * @param time pointer to timespec time
 * @param ns nanoseconds to be added
 */
static inline void add_time(struct timespec *time, long ns)
{
  if (!time || (ns <= 0))
    return;

  time->tv_nsec += ns;

  while (time->tv_nsec >= NS_PER_SEC)
  {
    time->tv_sec++;
    time->tv_nsec -= NS_PER_SEC;
  }
}

/**
 * @brief sleep for an interval specified with nanoseconds precision taking
 *        jitter into account
 * @param start pointer to the start time of the interval updated with the end
 *              time
 * @param interval interval duration in nanoseconds
 * @param jitter pointer to variation in sleep time to be considered if not
 *               NULL, updated with the latest one if necessary.
 *               It is automatically determined setting a negative value
 * __NOTE__: to sleep at regular intervals it is not needed to update the
 *           start time
 */
static inline void rt_thread_sleep(struct timespec *start,
                                   long interval,
                                   long *jitter)
{
  long max_jitter, curr_jitter = 0; // ns
  struct timespec resume_time = {0, 0};
  struct timespec *end = start;
  struct timespec sleep_time = *start;

  if (!start)
    return;

  add_time(end, interval);

  if (jitter)
  {
    // auto-adjust jitter
    max_jitter = (*jitter > 0 && interval > *jitter) ?
      *jitter :
      (*jitter = (interval >> 2));

    add_time(&sleep_time, (interval - max_jitter));
    clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &sleep_time, NULL);

    do // busy wait
    {
      clock_gettime(CLOCK_MONOTONIC, &resume_time);
    } while (resume_time.tv_sec < end->tv_sec ||
             (resume_time.tv_sec == end->tv_sec &&
              resume_time.tv_nsec < end->tv_nsec));

    curr_jitter = (resume_time.tv_sec - end->tv_sec) * NS_PER_SEC +
      (resume_time.tv_nsec - end->tv_nsec);

    if (curr_jitter > max_jitter)
      *jitter = curr_jitter;
  }
  else
    clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, end, NULL);
}

/**
 * @brief wrapper function to apply real-time policy to the thread routine
 */
static void *rt_thread_routine(void *arg)
{
  rt_thread_t *th = (rt_thread_t *) arg;
  int sig = 0;

  struct timespec start = {0, 0};
  const struct timespec delay = {
    .tv_sec = 0,
    .tv_nsec = 100000 // 10 kHz (with F_CPU = 1 GHz)
  };

  while (atomic_load(&th->started))
  {
    clock_gettime(CLOCK_MONOTONIC, &start);

    while (atomic_load(&th->started) && atomic_load(&th->interval))
    {
      if (atomic_load(&th->paused))
      {
        const int ret = sigwait(&th->sig_set, &sig);
        if (ret)
        {
          const char *err = NULL;
          get_err_descr(err, ret, "sigwait");
          fputs(err, stderr);
          free(err);
        }
        continue;
      }

      // execute routine
      th->routine();

      rt_thread_sleep(&start, atomic_load(&th->interval),
                      atomic_load(&th->accurate_sleep) ? &th->max_jitter : NULL);
    }

    while (atomic_load(&th->started) && !atomic_load(&th->interval))
      nanosleep(&delay, NULL);
  };

  return NULL;
}

rt_thread_t *rt_thread_start(rt_thread_function_t routine,
                             int priority,
                             char **error)
{
  if (error != NULL)
    *error = NULL;

  if (!routine)
  {
    get_err_descr(error, EINVAL, "thread routine: ");
    return NULL;
  }

  rt_thread_t *thread = (rt_thread_t *) malloc(sizeof(rt_thread_t));

  // init thread data
  thread->routine = routine;
  atomic_init(&thread->started, false);
  atomic_init(&thread->interval, 0); // disabled
  atomic_init(&thread->paused, false);
  atomic_init(&thread->accurate_sleep, false);
  thread->max_jitter = 0;

  // create thread
  pthread_attr_t attr;
  struct sched_param param;
  const int policy = priority > 0 ? SCHED_FIFO : SCHED_OTHER;
  param.sched_priority = priority > 0 ? priority : 0;

  int ret = pthread_attr_init(&attr);
  if (ret)
  {
    get_err_descr(error, ret, "pthread_attr_init: ");
    return NULL;
  }

  ret = pthread_attr_setschedpolicy(&attr, policy);
  if (ret)
  {
    get_err_descr(error, ret, "pthread_attr_setschedpolicy: ");
    return NULL;
  }

  ret = pthread_attr_setschedparam(&attr, &param);
  if (ret)
  {
    get_err_descr(error, ret, "pthread_attr_setschedparam: ");
    return NULL;
  }

  ret = pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
  if (ret)
  {
    get_err_descr(error, ret, "pthread_attr_setscope: ");
    return NULL;
  }

  ret = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
  if (ret)
  {
    get_err_descr(error, ret, "pthread_attr_setinheritsched: ");
    return NULL;
  }

  sigemptyset(&thread->sig_set);
  if (sigaddset(&thread->sig_set, RESUME_SIGNAL) == -1)
  {
    get_err_descr(error, errno, "sigaddset: ");
    return NULL;
  }

  ret = pthread_sigmask(SIG_BLOCK, &thread->sig_set, NULL);
  if (ret)
  {
    get_err_descr(error, ret, "pthread_sigmask: ");
    return NULL;
  }

  atomic_store(&thread->started, true);
  ret = pthread_create(&thread->id, &attr, rt_thread_routine, thread);
  if (ret)
  {
    atomic_store(&thread->started, false);
    get_err_descr(error, ret, "pthread_create: ");
    return NULL;
  }
  pthread_attr_destroy(&attr);

  // determine the max sleep-time jitter (using 100 us)
  long curr_jitter = 0;
  struct timespec start, end, calib_time = {.tv_sec = 0, .tv_nsec = 100000};
  for (int i = 0; i < JITTER_CALIB_CYCLES; i++)
  {
    clock_gettime(CLOCK_MONOTONIC, &start);
    clock_nanosleep(CLOCK_MONOTONIC, 0, &calib_time, NULL);
    clock_gettime(CLOCK_MONOTONIC, &end);

    curr_jitter = (end.tv_sec - start.tv_sec) * NS_PER_SEC +
      (end.tv_nsec - start.tv_nsec) - calib_time.tv_nsec;
    if (curr_jitter > thread->max_jitter)
      thread->max_jitter = curr_jitter;
  }

  return thread;
}

void rt_thread_enable(rt_thread_t *thread, int32_t interval, bool accurate)
{
  if (thread)
  {
    atomic_store(&thread->accurate_sleep, accurate);
    atomic_store(&thread->interval, interval > 0 ? interval : 0);
  }
}

bool rt_thread_stop(rt_thread_t **thread, char **error)
{
  if (error != NULL)
    *error = NULL;

  rt_thread_t *th = *thread;
  if (!th)
    return false;
  else if (!atomic_load(&th->started)) // already stopped
    return true;

  atomic_store(&th->interval, 0); // disabled
  atomic_store(&th->started, false);
  atomic_store(&th->paused, false);
  atomic_store(&th->accurate_sleep, false);

  const int ret = pthread_join(th->id, NULL);
  if (ret)
  {
    get_err_descr(error, ret, "pthread_join: ");
    return false;
  }

  free(*thread);
  *thread = NULL;
  return true;
}

void rt_thread_suspend(rt_thread_t *thread, bool suspend, char **error)
{
  atomic_store(&thread->paused, suspend);

  if (!suspend)
  {
    const int ret = pthread_kill(thread->id, RESUME_SIGNAL);
    if (ret)
      get_err_descr(error, ret, "pthread_kill: ");
  }
}
