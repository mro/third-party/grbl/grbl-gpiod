/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-06-20T14:03:01
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
**
** @details this file implements a resolver stub, there's as many resolvers as
** axis
*/

#include "resolvers.h"

#include <grbl/settings.h>
#include <grbl/state_machine.h>

#include "utils.h"

#define SETTING_RESOLVER_STEP_LOST_ENABLE Setting_AxisExtended2
#define SETTING_RESOLVER_STEP_LOST Setting_AxisExtended3
#define SETTING_RESOLVER_STEP_LOST_THRESHOLD Setting_AxisExtended4
#define SETTING_RESOLVER_MODE Setting_AxisExtended5
#define SETTING_RESOLVER_POSITION Setting_AxisExtended6
#define SETTING_DRIVER_POSITION Setting_AxisExtended7

static char buffer[16];

struct Resolver
{
  bool enabled;
  bool step_lost;
  uint32_t threshold;
  int32_t mode;
  int32_t position;
};
static struct Resolver s_resolvers[N_AXIS];

static status_code_t set_resolver_enable(setting_id_t id,
                                         uint_fast16_t int_value)
{
  if (id >= SETTING_RESOLVER_STEP_LOST_ENABLE &&
      id <= SETTING_RESOLVER_STEP_LOST_ENABLE + N_AXIS)
  {
    s_resolvers[id - SETTING_RESOLVER_STEP_LOST_ENABLE].enabled = int_value ?
      true :
      false;
    return Status_OK;
  }

  return Status_InvalidStatement;
}

static uint32_t get_resolver_enable(setting_id_t id)
{
  if (id >= SETTING_RESOLVER_STEP_LOST_ENABLE &&
      id <= SETTING_RESOLVER_STEP_LOST_ENABLE + N_AXIS)
    return s_resolvers[id - SETTING_RESOLVER_STEP_LOST_ENABLE].enabled;

  return UINT32_MAX;
}

static status_code_t set_step_lost(setting_id_t id, uint_fast16_t int_value)
{
  /* slots can only be cleared */
  if (!int_value && id >= SETTING_RESOLVER_STEP_LOST &&
      id <= SETTING_RESOLVER_STEP_LOST + N_AXIS)
  {
    s_resolvers[id - SETTING_RESOLVER_STEP_LOST].step_lost = false;
    return Status_OK;
  }

  return Status_InvalidStatement;
}

static uint32_t get_step_lost(setting_id_t id)
{
  if (id >= SETTING_RESOLVER_STEP_LOST &&
      id <= SETTING_RESOLVER_STEP_LOST + N_AXIS)
    return s_resolvers[id - SETTING_RESOLVER_STEP_LOST].step_lost;

  return UINT32_MAX;
}

static status_code_t set_resolver_slost_threshold(setting_id_t id, char *value)
{
  if (id >= SETTING_RESOLVER_STEP_LOST_THRESHOLD &&
      id <= SETTING_RESOLVER_STEP_LOST_THRESHOLD + N_AXIS)
  {
    uint32_t threshold;
    if (!read_uint32(value, &threshold))
      return Status_InvalidStatement;

    s_resolvers[id - SETTING_RESOLVER_STEP_LOST_THRESHOLD].threshold = threshold;
    return Status_OK;
  }
  return Status_InvalidStatement;
}

static char *get_resolver_slost_threshold(setting_id_t id)
{
  if (id >= SETTING_RESOLVER_STEP_LOST_THRESHOLD &&
      id <= SETTING_RESOLVER_STEP_LOST_THRESHOLD + N_AXIS)
    return uitoa(
      s_resolvers[id - SETTING_RESOLVER_STEP_LOST_THRESHOLD].threshold);

  buffer[0] = '\0';
  return buffer;
}

status_code_t set_resolver_mode(setting_id_t id, char *value)
{
  if (id >= SETTING_RESOLVER_MODE && id <= SETTING_RESOLVER_MODE + N_AXIS)
  {
    int32_t mode;
    if (!read_int32(value, &mode))
      return Status_InvalidStatement;

    s_resolvers[id - SETTING_RESOLVER_MODE].mode = mode;
    return Status_OK;
  }
  return Status_InvalidStatement;
}

static char *get_resolver_mode(setting_id_t id)
{
  if (id >= SETTING_RESOLVER_MODE && id <= SETTING_RESOLVER_MODE + N_AXIS)
  {
    write_int32(buffer, s_resolvers[id - SETTING_RESOLVER_MODE].mode);
  }
  else
    buffer[0] = '\0';
  return buffer;
}

status_code_t set_resolver_position(setting_id_t id, char *value)
{
  if (id >= SETTING_RESOLVER_POSITION &&
      id <= SETTING_RESOLVER_POSITION + N_AXIS)
  {
    int32_t position;
    if (!read_int32(value, &position))
      return Status_InvalidStatement;

    s_resolvers[id - SETTING_RESOLVER_POSITION].position = position;
    return Status_OK;
  }
  return Status_InvalidStatement;
}

static char *get_resolver_position(setting_id_t id)
{
  if (id >= SETTING_RESOLVER_POSITION &&
      id <= SETTING_RESOLVER_POSITION + N_AXIS)
  {
    write_int32(buffer, s_resolvers[id - SETTING_RESOLVER_POSITION].position);
  }
  else
    buffer[0] = '\0';
  return buffer;
}

status_code_t set_driver_position(setting_id_t id, char *value)
{
  if (id >= SETTING_DRIVER_POSITION && id <= SETTING_DRIVER_POSITION + N_AXIS)
  {
    uint8_t driver = id - SETTING_DRIVER_POSITION;
    // if motor is not idle or not on this instance
    sys_state_t state = state_get();
    if (state != STATE_IDLE && state != STATE_ALARM)
      return Status_IdleError;

    int32_t position;
    if (!read_int32(value, &position))
      return Status_InvalidStatement;

    sys.position[driver] = position;
    sync_position();
    return Status_OK;
  }
  return Status_InvalidStatement;
}

static char *get_driver_position(setting_id_t id)
{
  if (id >= SETTING_DRIVER_POSITION && id <= SETTING_DRIVER_POSITION + N_AXIS)
    write_int32(buffer, sys.position[id - SETTING_DRIVER_POSITION]);
  else
    buffer[0] = '\0';
  return buffer;
}

static const setting_detail_t _settings[] = {
  {SETTING_RESOLVER_STEP_LOST_ENABLE, Group_Axis0, "?-axis resolver enable",
   NULL, Format_Bool, NULL, NULL, NULL, Setting_IsExtendedFn,
   set_resolver_enable, get_resolver_enable, NULL},
  {SETTING_RESOLVER_STEP_LOST, Group_Axis0, "?-axis step-lost", NULL,
   Format_Bool, NULL, NULL, NULL, Setting_IsExtendedFn, set_step_lost,
   get_step_lost, NULL},
  {SETTING_RESOLVER_STEP_LOST_THRESHOLD, Group_Axis0,
   "?-axis resolver step-lost threshold", NULL, Format_String, NULL, NULL, NULL,
   Setting_IsExtendedFn, set_resolver_slost_threshold,
   get_resolver_slost_threshold, NULL},
  {SETTING_RESOLVER_MODE, Group_Axis0, "?-axis resolver mode", NULL,
   Format_String, NULL, NULL, NULL, Setting_IsExtendedFn, set_resolver_mode,
   get_resolver_mode, NULL},
  {SETTING_RESOLVER_POSITION, Group_Axis0, "?-axis resolver position", NULL,
   Format_String, NULL, NULL, NULL, Setting_IsExtendedFn, set_resolver_position,
   get_resolver_position, NULL},
  {SETTING_DRIVER_POSITION, Group_Axis0, "?-axis driver position", NULL,
   Format_String, NULL, NULL, NULL, Setting_IsExtendedFn, set_driver_position,
   get_driver_position, NULL}};
#define SETTINGS_SIZE (sizeof(_settings) / sizeof(_settings[0]))

static void resolvers_restore()
{
  for (size_t axis = 0; axis < N_AXIS; ++axis)
  {
    s_resolvers[axis].enabled = false;
    s_resolvers[axis].mode = 400;
    s_resolvers[axis].position = 0;
    s_resolvers[axis].step_lost = false;
    s_resolvers[axis].threshold = 50;
  }
}

static setting_details_t setting_details = {
  .settings = (const setting_detail_t *) &_settings,
  .n_settings = SETTINGS_SIZE,
  .restore = resolvers_restore};

void resolvers_init()
{
  resolvers_restore();
  settings_register(&setting_details);
}
