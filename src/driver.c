/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-03-17
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#include "driver.h"

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "local-config.h"
#ifdef LIBGPIOD
#include "gpio.h"
#endif
#include "grbl/hal.h"
#include "io_streams.h"
#include "rt_thread.h"
#include "settings.h"
#include "triggers.h"
#include "utils.h"

#ifdef VIRTUAL_LIMITS
#include "virtual_limits.h"
#endif

static struct
{
  pthread_mutex_t mutex;
  unsigned long long timeout; // monotonic time in nanoseconds
  delay_callback_ptr cb;
} delayed_callback = {.mutex = PTHREAD_MUTEX_INITIALIZER,
                      .timeout = 0,
                      .cb = NULL};

static rt_thread_t *stepper_thread = NULL;
static rt_thread_t *systick_thread = NULL;
static inline uint32_t hal_cycles_to_nanoseconds(uint32_t cycles)
{
  // The CPU cycle duration is equal to 1 ns with a CPU freq. of 1 GHz.
  return cycles; // = nanoseconds
}

// Driver
static bool hal_driver_setup(settings_t *settings);
static bool hal_driver_release();
static void hal_settings_changed(settings_t *settings);
static void hal_driver_delay_ms(uint32_t ms, void (*callback)(void));
static void hal_driver_on_execute_realtime(sys_state_t state);
static void hal_driver_on_state_change(sys_state_t state);

// Stepper
static long pulse_length = 0; // microseconds
static void hal_stepper_handler();
static void hal_stepper_wake_up();
static void hal_stepper_go_idle(bool clear_signals);
static void hal_stepper_cycles_per_tick(uint32_t cycles_per_tick);
static void hal_stepper_enable(axes_signals_t enable);
static void hal_stepper_pulse_start(stepper_t *stepper);

static inline void hal_set_step_outputs(axes_signals_t step_outbits_0);
static inline void hal_set_dir_outputs(axes_signals_t dir_outbits);

// I/O streams
void on_stream_exit(const char *err);

// Grbl - system
static void hal_systick_handler();
static pthread_mutex_t bits_ops_mutex = PTHREAD_MUTEX_INITIALIZER;
static inline void hal_bits_set_atomic(volatile uint_fast16_t *ptr,
                                       uint_fast16_t bits);
static inline uint_fast16_t hal_bits_clear_atomic(volatile uint_fast16_t *ptr,
                                                  uint_fast16_t bits);
static inline uint_fast16_t hal_value_set_atomic(volatile uint_fast16_t *ptr,
                                                 uint_fast16_t value);

// Spindle
static spindle_state_t hal_spindle_get_state();
static void hal_spindle_set_state(spindle_state_t state, float rpm);
#ifdef SPINDLE_PWM_DIRECT
static uint_fast16_t hal_spindle_get_pwm(float rpm);
static void hal_spindle_update_pwm(uint_fast16_t pwm_value);
#else
static void spindleUpdateRPM(float rpm);
#endif

// Limits
static void hal_limits_enable(bool on, bool homing);
static limit_signals_t hal_limits_get_state();

// Control
static control_signals_t hal_system_get_state();

// Coolant
static coolant_state_t hal_coolant_get_state();
static void hal_coolant_set_state(coolant_state_t mode);

/**** DRIVER ****/

bool driver_init()
{
  grbl.on_execute_realtime = hal_driver_on_execute_realtime;
  grbl.on_state_change = hal_driver_on_state_change;

  hal.info = "grbl-gpiod";
  hal.rx_buffer_size = RX_BUFFER_SIZE;
  hal.f_step_timer = 1000000000; // 1 GHz
  hal.delay_ms = hal_driver_delay_ms;

  hal.driver_version = "211121";
  hal.driver_setup = hal_driver_setup;
  hal.driver_release = hal_driver_release;
  hal.settings_changed = hal_settings_changed;

  hal.stepper.wake_up = hal_stepper_wake_up;
  hal.stepper.go_idle = hal_stepper_go_idle;
  hal.stepper.enable = hal_stepper_enable;
  hal.stepper.cycles_per_tick = hal_stepper_cycles_per_tick;
  hal.stepper.pulse_start = hal_stepper_pulse_start;

  memcpy(&hal.stream, io_streams_init(on_stream_exit), sizeof(io_stream_t));

  hal.nvs.type = NVS_None;

  hal.limits.enable = hal_limits_enable;
  hal.limits.get_state = hal_limits_get_state;

  hal.coolant.set_state = hal_coolant_set_state;
  hal.coolant.get_state = hal_coolant_get_state;

  hal.set_bits_atomic = hal_bits_set_atomic;
  hal.clear_bits_atomic = hal_bits_clear_atomic;
  hal.set_value_atomic = hal_value_set_atomic;

  hal.spindle.get_state = hal_spindle_get_state;
  hal.spindle.set_state = hal_spindle_set_state;
#ifdef SPINDLE_PWM_DIRECT
  hal.spindle.get_pwm = hal_spindle_get_pwm;
  hal.spindle.update_pwm = hal_spindle_update_pwm;
#else
  hal.spindle.update_rpm = spindleUpdateRPM;
#endif

  hal.control.get_state = hal_system_get_state;

  // driver capabilities
  hal.driver_cap.amass_level = 3;

  return hal.version == 9;
}

static bool hal_driver_setup(settings_t *settings)
{
  char *err = NULL;
  // Start the system tick thread meant to execute any delayed callback
  systick_thread = rt_thread_start(hal_systick_handler, 0, &err);
  if (err)
  {
    fputs(err, stderr);
    free(err);
    return false;
  }
  rt_thread_enable(systick_thread, hal_cycles_to_nanoseconds(1000000), // 1 ms
                   false);

  // Start the real-time thread which handles the stepper ISR
  // NOTE: root priviledges are required

  stepper_thread = rt_thread_start(hal_stepper_handler, set.priority, &err);
  if (err)
  {
    fputs(err, stderr);
    free(err);
    return false;
  }
  triggers_init(stepper_thread);

#ifdef VIRTUAL_LIMITS
  vlimits_init();
#endif

#ifdef RESOLVERS
  resolvers_init();
#endif

#ifdef LIBGPIOD
  gpio_setup(set.gpio_config, &err);
  if (err)
  {
    fputs(err, stderr);
    free(err);
    return false;
  }
#endif

  io_streams_open(set.input_stream, set.output_stream, &err);
  if (err)
  {
    fputs(err, stderr);
    free(err);
    return false;
  }

  hal.settings_changed(settings);
  hal.stepper.go_idle(true);

  return settings->version == 21;
}

static bool hal_driver_release()
{
  char *err = NULL;
  rt_thread_stop(&stepper_thread, &err);
  if (err)
  {
    fputs(err, stderr);
    free(err);
    err = NULL;
  }

#ifdef LIBGPIOD
  gpio_release();
#endif

  io_streams_close(&err);
  if (err)
  {
    fputs(err, stderr);
    free(err);
    err = NULL;
  }

  rt_thread_stop(&systick_thread, &err);
  if (err)
  {
    fputs(err, stderr);
    free(err);
    err = NULL;
  }

  return false; // exit from GRBL (re-)init loop
}

static void hal_settings_changed(settings_t *settings)
{
  pulse_length = (long) settings->steppers.pulse_microseconds;
  hal.max_step_rate = 1000000 / (pulse_length * 2); // 50% duty cycle

  float max_step_rate = hal.max_step_rate * 60.0f; // step rate per minute
  for (int idx = 0; idx < N_AXIS; idx++)
    if ((settings->axis[idx].steps_per_mm * settings->axis[idx].max_rate) >
        max_step_rate)
    {
      hal.stream.write("Warning: ");
      hal.stream.write(axis_letter[idx]);
      hal.stream.write("-axis step rate exceeds the max step rate\n");
    }
}

static void hal_driver_delay_ms(uint32_t ms, void (*callback)(void))
{
  if ((ms > 0) && !callback)
    micro_sleep(ms * 1000);
  else if ((ms > 0) && callback) // delay callback
  {
    struct timespec ts = {0, 0};
    clock_gettime(CLOCK_MONOTONIC, &ts); // now

    const int ret = pthread_mutex_lock(&delayed_callback.mutex);
    if (ret != 0)
    {
      char *err = NULL;
      get_err_descr(&err, ret,
                    "[Driver] Error locking mutex trying to set callback: ");
      fputs(err, stderr);
      free(err);
      return;
    }

    delayed_callback.cb = callback;
    delayed_callback.timeout = (ts.tv_sec * 1000000000) + ts.tv_nsec +
      (ms * 1000000);
    pthread_mutex_unlock(&delayed_callback.mutex);
  }
  else if (callback)
    callback();
}

static void hal_driver_on_execute_realtime(sys_state_t state)
{
  /* prepare steps before sleeping */
  if (state &
      (STATE_CYCLE | STATE_HOLD | STATE_SAFETY_DOOR | STATE_HOMING |
       STATE_SLEEP | STATE_JOG))
    st_prep_buffer();
  /* continue to fetch and execute rt commands, also used as a sleep */
  io_streams_fetch();
}

static void hal_driver_on_state_change(sys_state_t state)
{
  report_message("Stepper:StateChanged", Message_Plain);
}

/**** STEPPER ****/

static void hal_stepper_handler()
{
  hal.stepper.interrupt_callback();
}

static void hal_stepper_wake_up()
{
  rt_thread_enable(stepper_thread, hal_cycles_to_nanoseconds(5000), false);
}

static void hal_stepper_go_idle(bool clear_signals)
{
  rt_thread_enable(stepper_thread, 0, false);

  if (clear_signals)
  {
    hal_set_step_outputs((axes_signals_t){0});
    hal_set_dir_outputs((axes_signals_t){0});
  }
#ifdef VIRTUAL_LIMITS
  vlimits_stepper_mask(NULL);
#endif
}

static void hal_stepper_cycles_per_tick(uint32_t cycles_per_tick)
{
  rt_thread_enable(stepper_thread, hal_cycles_to_nanoseconds(cycles_per_tick),
                   true);
}

static void hal_stepper_enable(axes_signals_t enable)
{
#ifdef LIBGPIOD
  char *err = NULL;
  gpio_set(&gpio[STEP_ENABLE],
           enable.value ^ settings.steppers.enable_invert.mask, &err);
  if (err)
  {
    fputs(err, stderr);
    free(err);
  }
#endif
}

static void hal_stepper_pulse_start(stepper_t *stepper)
{
  if (stepper->new_block)
  {
    stepper->new_block = false;
    hal_set_dir_outputs(stepper->dir_outbits);
#ifdef VIRTUAL_LIMITS
    vlimits_stepper_mask(stepper);
#endif
  }

  if (stepper->step_outbits.value)
  {
    hal_set_step_outputs(stepper->step_outbits);

    micro_sleep(pulse_length);

    hal_set_step_outputs((axes_signals_t){0});
  }
}

static inline void hal_set_step_outputs(axes_signals_t step_outbits_0)
{
  step_outbits_0.mask = step_outbits_0.mask ^ settings.steppers.step_invert.mask;
#ifdef LIBGPIOD
  char *err = NULL;
  gpio_set(&gpio[STEP_PULSE], step_outbits_0.mask, &err);
  if (err)
  {
    fputs(err, stderr);
    free(err);
  }
#endif
}

static inline void hal_set_dir_outputs(axes_signals_t dir_outbits)
{
#ifdef LIBGPIOD
  char *err = NULL;
  gpio_set(&gpio[STEP_DIR],
           dir_outbits.value ^ settings.steppers.dir_invert.mask, &err);
  if (err)
  {
    fputs(err, stderr);
    free(err);
  }
#endif
}

/**** I/O streams ****/

void on_stream_exit(const char *err)
{
  if (err)
    fputs(err, stderr);
}

/**** System ****/
// Helper functions for setting/clearing/inverting individual bits atomically
static inline void hal_bits_set_atomic(volatile uint_fast16_t *ptr,
                                       uint_fast16_t bits)
{
  const int ret = pthread_mutex_lock(&bits_ops_mutex);
  if (ret != 0)
  {
    char *err = NULL;
    get_err_descr(&err, ret,
                  "[Driver] Error locking the mutex trying to set bits: ");
    fputs(err, stderr);
    free(err);
    return;
  }

  *ptr |= bits;
  pthread_mutex_unlock(&bits_ops_mutex);
}

static inline uint_fast16_t hal_bits_clear_atomic(volatile uint_fast16_t *ptr,
                                                  uint_fast16_t bits)
{
  const int ret = pthread_mutex_lock(&bits_ops_mutex);
  if (ret != 0)
  {
    char *err = NULL;
    get_err_descr(&err, ret,
                  "[Driver] Error locking the mutex trying to clear bits: ");
    fputs(err, stderr);
    free(err);
    return 0;
  }

  uint_fast16_t prev = *ptr;
  *ptr &= ~bits;
  pthread_mutex_unlock(&bits_ops_mutex);

  return prev;
}

static inline uint_fast16_t hal_value_set_atomic(volatile uint_fast16_t *ptr,
                                                 uint_fast16_t value)
{
  const int ret = pthread_mutex_lock(&bits_ops_mutex);
  if (ret != 0)
  {
    char *err = NULL;
    get_err_descr(&err, ret,
                  "[Driver] Error locking the mutex trying to set value: ");
    fputs(err, stderr);
    free(err);
    return 0;
  }

  uint_fast16_t prev = *ptr;
  *ptr = value;
  pthread_mutex_unlock(&bits_ops_mutex);

  return prev;
}

static void hal_systick_handler()
{
  static delay_callback_ptr callback = NULL;

  struct timespec ts = {0, 0};
  clock_gettime(CLOCK_MONOTONIC, &ts); // now
  const unsigned long long now = ts.tv_sec * 1000000000 + ts.tv_nsec;

  const int ret = pthread_mutex_lock(&delayed_callback.mutex);
  if (ret != 0)
  {
    char *err = NULL;
    get_err_descr(&err, ret,
                  "[Driver] Error locking mutex trying to execute callback: ");
    fputs(err, stderr);
    free(err);
    return;
  }

  callback = delayed_callback.cb;

  if (callback && (now >= delayed_callback.timeout))
  {
    delayed_callback.cb = NULL; // callback will be executed
    pthread_mutex_unlock(&delayed_callback.mutex);

    callback();
  }
  else
    pthread_mutex_unlock(&delayed_callback.mutex);
}

/**** Limits *****/ // Not used

static void hal_limits_enable(bool on, bool homing)
{}

static limit_signals_t hal_limits_get_state()
{
  limit_signals_t signals = {.min.mask = 0,
                             .max.mask = 0,
                             .min2.mask = 0,
                             .max2.mask = 0};
  return signals;
}

/**** Spindle ****/ // Not used
static spindle_state_t hal_spindle_get_state()
{
  spindle_state_t state = {0};
  return state;
}

static void hal_spindle_set_state(spindle_state_t state, float rpm)
{}

#ifdef SPINDLE_PWM_DIRECT

static uint_fast16_t hal_spindle_get_pwm(float rpm)
{
  return 0;
}

static void hal_spindle_update_pwm(uint_fast16_t pwm_value)
{}

#else

static void spindleUpdateRPM(float rpm)
{}

#endif

/**** Control ****/ // Not used
static control_signals_t hal_system_get_state()
{
  control_signals_t signals = {0};
  return signals;
}

/**** Coolant ****/ // Not used

static coolant_state_t hal_coolant_get_state()
{
  coolant_state_t state = {0};
  return state;
}

static void hal_coolant_set_state(coolant_state_t mode)
{}
