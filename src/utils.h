/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-03-09
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#ifndef UTILS_H
#define UTILS_H

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/**
 * @brief provide the string describing the error code
 * @param err pointer to the location where the error string is saved,
 *            if not NULL
 * @param errcode number of error
 * @param prefix any string to prepend to the error description
 * __NOTE__: the error location should be freed once it is no longer needed or
 *           if it will be re-used
 */
void get_err_descr(char **err, int errcode, const char *prefix);

/**
 * @brief sleep in microseconds
 * @param us microseconds
 */
void micro_sleep(long us);

/**
 * @brief check if the string begins with the given prefix
 * @param str string to inspect
 * @param pre string to search for
 * @return true on success, false on failure
 */
bool starts_with(const char *str, const char *pre);

/**
 * @brief short-hand for strcpy
 * @return strlen(str)
 */
static inline size_t nstrcpy(char *dest, const char *str)
{
  const size_t len = strlen(str);
  memcpy(dest, str, len);
  return len;
}

size_t read_int32(const char *str, int32_t *out);
size_t read_uint32(const char *str, uint32_t *out);
size_t write_int32(char *buffer, int32_t value);

#endif /* UTILS_H */
