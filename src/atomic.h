/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-08-01T13:33:55
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#ifndef ATOMIC_H
#define ATOMIC_H

#include <stdbool.h>

#include "local-config.h"

#if defined(HAS_STDATOMIC)
#include <stdatomic.h>

#elif defined(HAS_GCCATOMIC)

typedef bool atomic_bool;
typedef long atomic_long;

// NOLINTNEXTLINE(readability-identifier-naming)
#define atomic_load(v) __atomic_load_n(v, __ATOMIC_SEQ_CST)
// NOLINTNEXTLINE(readability-identifier-naming)
#define atomic_store(v, value) __atomic_store_n(v, value, __ATOMIC_SEQ_CST)
// NOLINTNEXTLINE(readability-identifier-naming)
#define atomic_init(v, value) \
  do                          \
  {                           \
    *(v) = value;             \
  } while (0)

#else
#error "no atomics support"
#endif

#endif
