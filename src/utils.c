/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-03-09
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#include "utils.h"

#include <grbl/hal.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define NS_PER_SEC 1000000000
#define ERR_STRING_SIZE 128

void get_err_descr(char **err, int errcode, const char *prefix)
{
  if (err == NULL)
    return;

  const size_t prefix_len = (prefix != NULL) ? strlen(prefix) : 0;
  const size_t string_len = prefix_len + ERR_STRING_SIZE + 1;
  *err = (char *) malloc(string_len);

  strcpy(*err, prefix_len > 0 ? prefix : "");

#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && !_GNU_SOURCE
  strerror_r(errcode, &(*err)[prefix_len], ERR_STRING_SIZE);
#else
  const char *str = strerror_r(errcode, &(*err)[prefix_len], ERR_STRING_SIZE);
  if ((*err)[prefix_len] == '\0') // error buffer unused
    strncat(*err, str, ERR_STRING_SIZE);
#endif
  strcat(*err, "\n");
}

void micro_sleep(long us)
{
  struct timespec ts = {.tv_sec = 0, .tv_nsec = us * 1000};

  while (ts.tv_nsec >= NS_PER_SEC)
  {
    ts.tv_sec++;
    ts.tv_nsec -= NS_PER_SEC;
  }
  nanosleep(&ts, NULL);
}

bool starts_with(const char *str, const char *pre)
{
  if (str == NULL || pre == NULL)
    return false;

  size_t str_len = strlen(str);
  size_t pre_len = strlen(pre);

  return (str_len < pre_len) ? false : (strncmp(str, pre, pre_len) == 0);
}

size_t read_int32(const char *str, int32_t *out)
{
  uint32_t uout;
  size_t pos;

  if (str[0] == '-')
  {
    pos = read_uint32(str + 1, &uout);
    if (pos)
    {
      *out = (int32_t) -uout;
      ++pos;
    }
  }
  else
  {
    pos = read_uint32(str, &uout);
    if (pos)
      *out = (int32_t) uout;
  }
  return pos;
}

size_t read_uint32(const char *str, uint32_t *out)
{
  uint32_t value = 0;
  size_t pos;

  for (pos = 0; str[pos] >= '0' && str[pos] <= '9'; ++pos)
  {
    value *= 10;
    value += str[pos] - '0';
  }
  if (pos)
    *out = value;
  return pos;
}

size_t write_int32(char *buffer, int32_t value)
{
  size_t pos = 0;
  if (value >= 0)
    pos += nstrcpy(buffer, uitoa((uint32_t) value));
  else
  {
    buffer[pos++] = '-';
    pos += nstrcpy(&buffer[1], uitoa((uint32_t) -value));
  }
  buffer[pos] = '\0';
  return pos;
}
