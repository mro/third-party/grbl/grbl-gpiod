/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-06-01T23:04:09+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef IOPORTS_H
#define IOPORTS_H

#include <stdint.h>

#include "grbl/hal.h"
#include "rt_thread.h"

#define TRIGGER_MASK(bit) (((uint_fast16_t) 1) << bit)

#define HW_TRIGGER_PIN 0
#define PAUSE_RESUME_PIN 1

void triggers_init(rt_thread_t *stepper);

void trigger_arm(uint_fast16_t mask);
void trigger_disarm(uint_fast16_t mask);
void trigger_trig(uint_fast16_t mask, bool hardware);

#endif /* IOPORTS_H */
