/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-03-07
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#include "io_streams.h"

#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "grbl/hal.h"
#include "grbl/nuts_bolts.h"
#include "grbl/protocol.h"
#include "grbl/stream.h"
#include "utils.h"

#define TMP_BUFFER_SIZE 20
#define POLL_TIMEOUT_MS 1

static stream_rx_buffer_t rxbuffer = {0};
static enqueue_realtime_command_ptr enqueue_realtime_command =
  protocol_enqueue_realtime_command;
static on_stream_error_ptr on_stream_exit = NULL;
static bool exit_cmd = false;

static struct
{
  int in;
  int out;
} file;

static int16_t read_char();
static inline uint16_t get_rx_buffer_count();
static inline uint16_t get_rx_buffer_free();
static inline void reset_rx_buffer();
static inline void cancel_rx_buffer();
static inline void write_n(const char *data, uint16_t len);
static bool write_char(const char c);
static void write_string(const char *data);
static bool suspend_read(bool suspend);
static enqueue_realtime_command_ptr set_rt_handler(
  enqueue_realtime_command_ptr handler);

static inline void on_stream_error_default(const char *err)
{
  if (err)
    fputs(err, stderr);
}

const io_stream_t *io_streams_init(on_stream_error_ptr cb)
{
  static const io_stream_t stream = {
    .type = StreamType_Serial,
    .state.connected = true,
    .read = read_char,
    .write_n = write_n,
    .write = write_string,
    .write_char = write_char,
    .write_all = write_string,
    .get_rx_buffer_free = get_rx_buffer_free,
    .get_rx_buffer_count = get_rx_buffer_count,
    .reset_read_buffer = reset_rx_buffer,
    .cancel_read_buffer = cancel_rx_buffer,
    .suspend_read = suspend_read,
    .set_enqueue_rt_handler = set_rt_handler,
    .enqueue_rt_command = protocol_enqueue_realtime_command};

  on_stream_exit = cb ? cb : on_stream_error_default;
  exit_cmd = false;
  return &stream;
}

bool io_streams_open(const char *in, const char *out, char **err)
{
  int ret = 0;
  if ((in != NULL) && (strlen(in) > 0))
    ret = file.in = open(in, O_RDONLY | O_NONBLOCK);
  else
  {
    file.in = STDIN_FILENO;
    const int flags = fcntl(file.in, F_GETFL, 0);
    ret = (flags < 0) ? -1 : fcntl(file.in, F_SETFL, flags | O_NONBLOCK);
  }

  if ((ret == -1) && (err != NULL))
  {
    get_err_descr(err, errno, "[IO streams] Error opening input file: ");
    return false;
  }

  if ((out != NULL) && (strlen(out) > 0))
    ret = file.out = open(out, O_WRONLY | O_NONBLOCK);
  else
  {
    file.out = STDOUT_FILENO;
    const int flags = fcntl(file.out, F_GETFL, 0);
    ret = (flags < 0) ? -1 : fcntl(file.out, F_SETFL, flags | O_NONBLOCK);
  }

  if ((ret == -1) && (err != NULL))
  {
    get_err_descr(err, errno, "[IO streams] Error opening output file: ");
    return false;
  }

  return true;
}

bool io_streams_close(char **err)
{
  bool ok = true;
  if ((close(file.in) < 0) && (err != NULL))
  {
    get_err_descr(err, errno, "[IO streams] Error closing input file: ");
    ok = false;
  }
  else
    file.in = -1;

  if ((close(file.out) < 0) && (err != NULL))
  {
    if (ok)
    {
      get_err_descr(err, errno, "[IO streams] Error closing output file: ");
      ok = false;
    }
    else
      get_err_descr(err, errno,
                    "[IO streams] Error closing input and output file: ");
  }
  else
    file.out = -1;

  return ok;
}

bool io_streams_fetch()
{
  char *err = NULL;
  char tmp[TMP_BUFFER_SIZE] = "";
  bool ret = (rxbuffer.head != rxbuffer.tail);

  if (!exit_cmd)
  {
    ssize_t nb = 0;
    struct pollfd pfd[1] = {{file.in, POLLIN, 0}};

    /* release pressure on main thread
      main loop execution must be < 40ms to ensure step buffer is not starving */
    if (poll(pfd, 1, POLL_TIMEOUT_MS) < 0)
    {
      get_err_descr(&err, errno, "[IO streams] Failed to poll for data: ");
      on_stream_exit(err);
      free(err);
      exit_cmd = true;
    }
    else
      nb = read(file.in, tmp, min(RX_BUFFER_SIZE, TMP_BUFFER_SIZE));

    if (nb > 0)
    {
      int i;
      // fill the RX buffer
      for (i = 0; i < nb; i++)
      {
        if (!hal.stream.enqueue_rt_command ||
            !hal.stream.enqueue_rt_command(tmp[i]))
        {
          uint_fast16_t next_head = BUFNEXT(rxbuffer.head, rxbuffer);
          if (rxbuffer.tail == next_head)
            rxbuffer.overflow = true;
          else
          {
            rxbuffer.data[rxbuffer.head] = tmp[i]; // add character to RX buffer
            rxbuffer.head = next_head;
            ret = true;
          }
        }
      }
    }
    else if ((nb < 0) && (errno != EAGAIN) && (errno != EWOULDBLOCK))
    {
      get_err_descr(&err, errno,
                    "[IO streams] Error reading from the input stream: ");
      on_stream_exit(err);
      free(err);
      exit_cmd = true;
    }
    else if (nb == 0) // EOF
    {
      on_stream_exit(NULL);
      exit_cmd = true;
    }
  }
  else
    enqueue_realtime_command(CMD_EXIT);

  return ret;
}

static int16_t read_char()
{
  if (rxbuffer.head != rxbuffer.tail)
  {
    uint16_t bptr = rxbuffer.tail;
    char data = rxbuffer.data[bptr++];
    rxbuffer.tail = bptr & (RX_BUFFER_SIZE - 1);

    return (int16_t) data;
  }
  else
    return SERIAL_NO_DATA;
}

static inline uint16_t get_rx_buffer_count()
{
  uint_fast16_t head = rxbuffer.head;
  uint_fast16_t tail = rxbuffer.tail;

  return BUFCOUNT(head, tail, RX_BUFFER_SIZE);
}

static inline uint16_t get_rx_buffer_free()
{
  return (RX_BUFFER_SIZE - 1) - get_rx_buffer_count();
}

static inline void reset_rx_buffer()
{
  rxbuffer.tail = rxbuffer.head;
}

static inline void cancel_rx_buffer()
{
  reset_rx_buffer();

  rxbuffer.data[rxbuffer.head] = ASCII_CAN;
  rxbuffer.head = (rxbuffer.tail + 1) % RX_BUFFER_SIZE;
}

static inline void write_n(const char *data, uint16_t len)
{
  ssize_t nb = 0; // number of bytes written
  char *err = NULL;

  if ((data == NULL) || (len == 0) || exit_cmd)
    return;

  do
  {
    nb = write(file.out, data, len);

    if (nb > 0)
      len -= nb;
    else if ((nb < 0) && (errno != EAGAIN) && (errno != EWOULDBLOCK))
    {
      get_err_descr(&err, errno,
                    "[IO streams] Error writing to the output stream: ");
      on_stream_exit(err);
      free(err);
      exit_cmd = true;
      return;
    }

    if ((len > 0) && hal.stream_blocking_callback())
      micro_sleep(10);
    else // forced exit
      len = 0;
  } while (len > 0);
}

static bool write_char(const char c)
{
  write_n(&c, 1);
  return true;
}

static void write_string(const char *data)
{
  write_n(data, (uint16_t) strlen(data));
}

static bool suspend_read(bool suspend)
{
  return stream_rx_suspend(&rxbuffer, suspend);
}

static enqueue_realtime_command_ptr set_rt_handler(
  enqueue_realtime_command_ptr handler)
{
  enqueue_realtime_command_ptr prev = enqueue_realtime_command;

  if (handler)
    enqueue_realtime_command = handler;

  return prev;
}
