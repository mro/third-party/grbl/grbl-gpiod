/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-03-13
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#include "gpio.h"

#include <errno.h>
#include <fcntl.h>
#include <search.h>
#include <stdio.h>
#include <string.h>

#include "utils.h"

#define MAX_LINE_LEN 256
#define ERR_PREFIX_LEN 64

typedef struct
{
  char *name;
  struct gpiod_chip *ptr;
} gpio_open_chip_t;

gpio_cluster_t gpio[N_PIN_CLUSTER];
static int num_chips = 0;
static gpio_open_chip_t *open_chips = NULL;
static char *line_bulk_prefixes[] = {"STEP_", "DIR_", "EN_"};

static bool gpio_parse_config_file(const char *path, char **err);
static bool gpio_bind(const char *pin, const char *chip, int line, char **err);
static void gpio_cluster_setup(size_t index, uint32_t num_lines);
static bool gpio_request_lines(char **err);
static struct gpiod_chip *gpio_get_chip(const char *descr);

bool gpio_setup(const char *config, char **err)
{
  // GPIO chip device check
  struct gpiod_chip_iter *iter = gpiod_chip_iter_new();
  struct gpiod_chip *chip;
  if (iter == NULL)
  {
    get_err_descr(err, errno, "[GPIO] Error searching chips: ");
    return false;
  }

  gpiod_foreach_chip(iter, chip)
  {
    num_chips++;
  };

  // init
  memset(&gpio, 0, sizeof(gpio_cluster_t) * N_PIN_CLUSTER);

  open_chips = (gpio_open_chip_t *) calloc(sizeof(gpio_open_chip_t), num_chips);

  if ((num_chips > 0) && // do not conclude setup if there are no chips
      (!gpio_parse_config_file(config, err) || !gpio_request_lines(err)))
    return false;

  return true;
}

void gpio_release()
{
  int i = 0;

  while (open_chips && open_chips[i].ptr && i < num_chips)
  {
    free(open_chips[i].name);
    gpiod_chip_close((struct gpiod_chip *) open_chips[i].ptr);
    i++;
  }
  free(open_chips);

  for (i = 0; i < N_PIN_CLUSTER; i++)
  {
    struct gpio_chip_t *temp, *head = gpio[i].chip_list;
    while (head)
    {
      temp = head;
      head = temp->next;
      free(temp->bulk);
      free(temp->values);
      free(temp);
    }
  }
}

// populate the gpio array with info from the GPIO config file
static bool gpio_parse_config_file(const char *path, char **err)
{
  char config_line[MAX_LINE_LEN] = {0};
  FILE *config = fopen(
    (path != NULL) && (strlen(path) > 0) ? path : "gpio.conf", "r");

  if (!config)
  {
    get_err_descr(err, errno, "[GPIO] Error opening config file: ");
    return false;
  }

  // parse lines with format <pin>:<chip>/<line>
  while (fgets(config_line, MAX_LINE_LEN, config))
  {
    // extract pin name
    const char *pin = strtok(config_line, ":");

    // extract gpio chip and line
    const char *gpio_chip_descr = strtok(NULL, "\n"); // includes 'line'
    char *gpio_line = strrchr(gpio_chip_descr, '/');
    if (!gpio_line)
    {
      fprintf(stderr, "[GPIO] Error parsing chip/line '%s': Invalid argument\n",
              gpio_chip_descr);
      continue;
    }

    *(gpio_line++) = '\0'; // overwrite with a '\0' and point to the next byte

    errno = 0;
    char *str_end;
    const long n_line = strtol(gpio_line, &str_end, 10);
    if (errno != 0 || str_end == gpio_line)
    {
      fprintf(stderr, "[GPIO] Error parsing line '%s': Invalid argument\n",
              gpio_line);
      continue;
    }

    char *err = NULL;
    gpio_bind(pin, gpio_chip_descr, n_line, &err);
    if (err)
    {
      fputs(err, stderr);
      free(err);
    }
  }

  if (fclose(config))
  {
    get_err_descr(err, errno, "[GPIO] Error closing config file: ");
    return false;
  }

  return true;
}

// reserve GPIO lines from configuration
static bool gpio_request_lines(char **err)
{
  int cluster_idx;
  for (cluster_idx = 0; cluster_idx < N_PIN_CLUSTER; cluster_idx++)
  {
    switch (cluster_idx)
    {
    case STEP_ENABLE:
    case STEP_DIR:
    case STEP_PULSE:
    {
      gpio_cluster_setup(cluster_idx, N_AXIS);

      const int low[N_AXIS] = {0};

      for (struct gpio_chip_t *chip = gpio[cluster_idx].chip_list;
           chip && chip->bulk; chip = chip->next)
      {
        if (gpiod_line_request_bulk_output(chip->bulk, "grbl-gpiod", low) != 0)
        {
          char prefix[ERR_PREFIX_LEN];
          snprintf(prefix, ERR_PREFIX_LEN,
                   "[GPIO] Error reserving '%s*' GPIO lines: ",
                   line_bulk_prefixes[cluster_idx]);
          get_err_descr(err, errno, prefix);
          return false;
        }
      }
      break;
    }
    default: break;
    }
  }
  return true;
}

// prepare a cluster for line reservation
static void gpio_cluster_setup(size_t index, uint32_t num_lines)
{
  int line_idx;
  struct gpio_chip_t *chip = gpio[index].chip_list;

  if (num_lines > N_AXIS)
    num_lines = N_AXIS;

  for (line_idx = 0; line_idx < num_lines; line_idx++)
  {
    struct gpiod_line *line_ptr = gpio[index].lines[line_idx];
    if (line_ptr)
    {
      // find the chip where to add it (if any)
      struct gpiod_line_bulk *bulk_ptr = NULL;
      const struct gpiod_chip *line_chip = gpiod_line_get_chip(line_ptr);
      while (!bulk_ptr)
      {
        // locate the bulk
        if (chip && chip->bulk)
        {
          struct gpiod_line *first_line = gpiod_line_bulk_get_line(chip->bulk,
                                                                   0);
          if (line_chip == gpiod_line_get_chip(first_line))
            bulk_ptr = chip->bulk;
          else
            chip = chip->next;
        }
        else
        {
          if (!chip) // create chip and insert it at the chip list head
          {
            chip = (struct gpio_chip_t *) malloc(sizeof(struct gpio_chip_t));
            chip->next = gpio[index].chip_list;
            gpio[index].chip_list = chip;
          }

          chip->bulk = bulk_ptr = (struct gpiod_line_bulk *) malloc(
            sizeof(struct gpiod_line_bulk));
          gpiod_line_bulk_init(chip->bulk);

          chip->values = (int *) malloc(sizeof(int) * num_lines);
        }
      }
      // link cluster value to the respective chip value
      gpio[index].values[line_idx] =
        &chip->values[gpiod_line_bulk_num_lines(chip->bulk)];

      gpiod_line_bulk_add(bulk_ptr, line_ptr);
    }
    else
      fprintf(stderr, "[GPIO] Missing or invalid '%s%s' pin\n",
              line_bulk_prefixes[index], axis_letter[line_idx]);
  }
}

// bind the logical pin to the physical GPIO line
static bool gpio_bind(const char *pin, const char *chip, int line, char **err)
{
  int cluster = -1, cluster_line;
  char axis;

  if (starts_with(pin, line_bulk_prefixes[STEP_PULSE]))
    cluster = STEP_PULSE;
  else if (starts_with(pin, line_bulk_prefixes[STEP_DIR]))
    cluster = STEP_DIR;
  else if (starts_with(pin, line_bulk_prefixes[STEP_ENABLE]))
    cluster = STEP_ENABLE;

  // axis letter should be in the last position of pin string
  axis = (cluster >= 0) ? pin[strlen(line_bulk_prefixes[cluster])] : '\0';
  switch (axis)
  {
  case 'X': cluster_line = X_AXIS; break;
  case 'Y': cluster_line = Y_AXIS; break;
  case 'Z': cluster_line = Z_AXIS; break;
#if N_AXIS > 3
  case 'A': cluster_line = A_AXIS; break;
#endif
#if N_AXIS > 4
  case 'B': cluster_line = B_AXIS; break;
#endif
#if N_AXIS > 5
  case 'C': cluster_line = C_AXIS; break;
#endif
#if N_AXIS > 6
  case 'U': cluster_line = U_AXIS; break;
#endif
#if N_AXIS > 7
  case 'V': cluster_line = V_AXIS; break;
#endif
  default:
  {
    char prefix[ERR_PREFIX_LEN];
    snprintf(prefix, ERR_PREFIX_LEN, "[GPIO] Error parsing pin '%s': ", pin);
    get_err_descr(err, EINVAL, prefix);
    return false;
  }
  }

  // get GPIO line and add it to the respective bulk
  struct gpiod_chip *chip_ptr = gpio_get_chip(chip);
  if (!chip_ptr)
  {
    get_err_descr(err, errno, "[GPIO] Error getting gpio chip: ");
    return false;
  }

  if (!(gpio[cluster].lines[cluster_line] = gpiod_chip_get_line(chip_ptr, line)))
  {
    get_err_descr(err, errno, "[GPIO] Error getting gpio line: ");
    return false;
  }
  return true;
}

static int gpio_chip_compare(const void *chip1, const void *chip2)
{
  gpio_open_chip_t *c1 = (gpio_open_chip_t *) chip1;
  gpio_open_chip_t *c2 = (gpio_open_chip_t *) chip2;
  return strcmp((const char *) c1->name, (const char *) c2->name);
}

// retrieve the chip handle by path (/dev/<chip>), name or label
static struct gpiod_chip *gpio_get_chip(const char *descr)
{
  static size_t i = 0; // number of open chips
  gpio_open_chip_t *chip, key = {.name = descr, .ptr = NULL};
  struct gpiod_chip *ret;

  if (!descr || (strlen(descr) == 0))
    ret = NULL;
  else if ((chip = lfind(&key, open_chips, &i, sizeof(gpio_open_chip_t),
                         gpio_chip_compare)) != NULL)
    ret = chip->ptr;
  else
  {
    open_chips[i].name = strdup(descr);
    open_chips[i].ptr = ret = gpiod_chip_open_lookup(descr);
    i++;
  }
  return ret;
}

void gpio_set(gpio_cluster_t *cluster, uint8_t pins, char **err)
{
  if (!cluster)
  {
    get_err_descr(err, EINVAL, "[GPIO] Cannot use bulk to set values: ");
    return;
  }

  // prepare values to set
  cluster->state = (cluster->state & ~AXES_BITMASK) | (pins & AXES_BITMASK);

  int i, *value_ptr;
  uint8_t state;
  for (i = 0, state = cluster->state; i < N_AXIS; i++, state >>= 1)
    if ((value_ptr = cluster->values[i])) // skip missing lines
      *value_ptr = (state & 0x01) ? On : Off;

  // set values
  for (struct gpio_chip_t *chip = cluster->chip_list;
       chip && chip->bulk && chip->values; chip = chip->next)
  {
    if (gpiod_line_set_value_bulk(chip->bulk, chip->values) != 0)
    {
      get_err_descr(err, errno, "[GPIO] Error setting the value: ");
      break;
    }
  }
}

uint8_t gpio_get(gpio_cluster_t *cluster, char **err)
{
  if (!cluster)
  {
    get_err_descr(err, EINVAL, "[GPIO] Cannot use bulk to get values: ");
    return 0;
  }

  // get values
  for (struct gpio_chip_t *chip = cluster->chip_list;
       chip && chip->bulk && chip->values; chip = chip->next)
  {
    if (gpiod_line_get_value_bulk(chip->bulk, chip->values) != 0)
    {
      get_err_descr(err, errno, "[GPIO] Error getting the value: ");
      return 0;
    }
  }

  // update the state
  int i, *value_ptr;
  uint8_t set_bit = 1;
  cluster->state = 0;
  for (i = 0; i < N_AXIS; i++, set_bit <<= 1)
    if ((value_ptr = cluster->values[i])) // skip unmapped lines
      cluster->state |= *value_ptr ? set_bit : 0;

  return cluster->state;
}
