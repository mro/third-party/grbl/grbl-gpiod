/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-03-07
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#ifndef IO_STREAM_H
#define IO_STREAM_H

#include "grbl/stream.h"

typedef void (*on_stream_error_ptr)(const char *err);

/**
 * @brief HAL I/O stream initialization
 * @param cb callback invoked when an error occurs during an I/O operation that
 *           causes grbl-core exit. The callback is passed the error message.
 * @return the I/O stream functions to be set up in 'driver_init()'
 */
const io_stream_t *io_streams_init(on_stream_error_ptr cb);

/**
 * @brief opens I/O streams
 * @param in input file name
 * @param out output file name
 * @param err pointer to the location where the eventual error message is
 *            copied, if not NULL
 * @return true if the streams opening succeeded, false otherwise
 */
bool io_streams_open(const char *in, const char *out, char **err);

/**
 * @brief closes I/O streams
 * @param err pointer to the location where the eventual error message is
 *            copied, if not NULL
 * @return true if the streams closure succeeded, false otherwise
 */
bool io_streams_close(char **err);

/**
 * @brief fetch input data
 *
 * @return true if there's pending data
 * @details does process realtime-commands
 */
bool io_streams_fetch();

#endif /* IO_STREAM_H */
