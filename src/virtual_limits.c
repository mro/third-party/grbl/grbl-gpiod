/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-10-04T18:48:14
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include "virtual_limits.h"

#include <stdint.h>
#include <string.h>

#include "driver.h"

// #define DEBUG
#ifdef DEBUG
#include <stdio.h>
#endif

typedef enum
{
  VLIMIT_PIN_X_MIN = 0,
  VLIMIT_PIN_Y_MIN = 1,
  VLIMIT_PIN_Z_MIN = 2,
  VLIMIT_PIN_A_MIN = 3,
  VLIMIT_PIN_B_MIN = 4,
  VLIMIT_PIN_C_MIN = 5,
  VLIMIT_PIN_U_MIN = 6,
  VLIMIT_PIN_V_MIN = 7,
  VLIMIT_PIN_MIN_LAST = VLIMIT_PIN_V_MIN,

  VLIMIT_PIN_X_MAX = 8,
  VLIMIT_PIN_Y_MAX = 9,
  VLIMIT_PIN_Z_MAX = 10,
  VLIMIT_PIN_A_MAX = 11,
  VLIMIT_PIN_B_MAX = 12,
  VLIMIT_PIN_C_MAX = 13,
  VLIMIT_PIN_U_MAX = 14,
  VLIMIT_PIN_V_MAX = 15,
  VLIMIT_PIN_LAST = VLIMIT_PIN_V_MAX
} vlimit_pin_t;
#define VLIMIT_PIN_MIN_MASK ((1 << (VLIMIT_PIN_MIN_LAST + 1)) - 1)
#define VLIMIT_PIN_MASK ((1 << (VLIMIT_PIN_LAST + 1)) - 1)

static uint8_t s_pin_index;
static volatile struct
{
  uint_fast16_t value; // port status
  uint_fast16_t imr;   // interrupt mask register: set to 1 to allow interrupts
  uint_fast16_t lmr;   // limits mask register: set to 1 to allor limit
} s_port;
static enumerate_pins_ptr on_enumerate_pins;
static io_port_t on_port;

static const periph_pin_t aux_out[] = {{.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_X_MIN,
                                        .group = PinGroup_AuxOutput,
                                        .description = "X-axis Limit Pin Min"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_Y_MIN,
                                        .group = PinGroup_AuxOutput,
                                        .description = "Y-axis Limit Pin Min"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_Z_MIN,
                                        .group = PinGroup_AuxOutput,
                                        .description = "Z-axis Limit Pin Min"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_A_MIN,
                                        .group = PinGroup_AuxOutput,
                                        .description = "A-axis Limit Pin Min"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_B_MIN,
                                        .group = PinGroup_AuxOutput,
                                        .description = "B-axis Limit Pin Min"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_C_MIN,
                                        .group = PinGroup_AuxOutput,
                                        .description = "C-axis Limit Pin Min"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_U_MIN,
                                        .group = PinGroup_AuxOutput,
                                        .description = "U-axis Limit Pin Min"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_V_MIN,
                                        .group = PinGroup_AuxOutput,
                                        .description = "V-axis Limit Pin Min"},

                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_X_MAX,
                                        .group = PinGroup_AuxOutput,
                                        .description = "X-axis Limit Pin Max"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_Y_MAX,
                                        .group = PinGroup_AuxOutput,
                                        .description = "Y-axis Limit Pin Max"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_Z_MAX,
                                        .group = PinGroup_AuxOutput,
                                        .description = "Z-axis Limit Pin Max"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_A_MAX,
                                        .group = PinGroup_AuxOutput,
                                        .description = "A-axis Limit Pin Max"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_B_MAX,
                                        .group = PinGroup_AuxOutput,
                                        .description = "B-axis Limit Pin Max"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_C_MAX,
                                        .group = PinGroup_AuxOutput,
                                        .description = "C-axis Limit Pin Max"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_U_MAX,
                                        .group = PinGroup_AuxOutput,
                                        .description = "U-axis Limit Pin Max"},
                                       {.function = Output_Aux1,
                                        .pin = VLIMIT_PIN_V_MAX,
                                        .group = PinGroup_AuxOutput,
                                        .description = "V-axis Limit Pin Max"}};
#define AUX_OUT_SIZE (sizeof(aux_out) / sizeof(aux_out[0]))

limit_signals_t limits_get_state()
{
  limit_signals_t signals = {0};
  uint16_t value = s_port.value & s_port.lmr;
  signals.min.value = value & VLIMIT_PIN_MIN_MASK;
  signals.max.value = (value >> (VLIMIT_PIN_MIN_LAST + 1)) & VLIMIT_PIN_MIN_MASK;
  return signals;
}

static void digital_out(uint8_t port, bool on)
{
  uint8_t index = port - s_pin_index;
  if (index >= AUX_OUT_SIZE)
  {
    if (on_port.digital_out)
      on_port.digital_out(port, on);
    return;
  }

#ifdef DEBUG
  fprintf(stderr, "port(%s) = %s\n", aux_out[index].description,
          on ? "on" : "off");
#endif

  uint16_t pin = (1 << index);
  if (on && !(s_port.value & pin))
  {
    s_port.value |= pin;
    if ((s_port.imr & s_port.lmr & pin) && hal.limits.interrupt_callback)
      hal.limits.interrupt_callback(limits_get_state());
  }
  else if (!on && (s_port.value & pin))
  {
    s_port.value &= ~pin;
  }
}

void limits_enable(bool on, bool homing)
{
  on &= settings.limits.flags.hard_enabled;
  if (on)
  {
    s_port.imr |= VLIMIT_PIN_MASK;
    if ((s_port.imr & s_port.lmr & s_port.value) &&
        hal.limits.interrupt_callback)
      hal.limits.interrupt_callback(limits_get_state());
  }
  else
  {
    s_port.imr &= ~VLIMIT_PIN_MASK;
  }
}

static xbar_t *get_pin_info(io_port_type_t type,
                            io_port_direction_t dir,
                            uint8_t port)
{
  static xbar_t pin;
  xbar_t *info = NULL;

  if (type == Port_Digital && dir == Port_Output && port < AUX_OUT_SIZE)
  {
    memset(&pin, 0, sizeof(xbar_t));
    pin.mode.output = On;
    pin.function = aux_out[port].function;
    pin.group = aux_out[port].group;
    pin.pin = aux_out[port].pin + s_pin_index;
    pin.bit = 1 << aux_out[port].pin;
    pin.port = (void *) aux_out[port].port;
    pin.description = aux_out[port].description;
    info = &pin;
  }
  else if (on_port.get_pin_info)
    return on_port.get_pin_info(type, dir, port);

  return info;
}

static void enumerate_pins(bool low_level, pin_info_ptr pin_info)
{
  size_t i;

  if (on_enumerate_pins)
    on_enumerate_pins(low_level, pin_info);

  for (i = 0; i < AUX_OUT_SIZE; ++i)
    pin_info(get_pin_info(Port_Digital, Port_Output, i));
}

static void vlimits_set_mask(uint16_t mask)
{
#ifdef DEBUG
  fprintf(stderr, "setting limits mask:%.4hx value:%.4hx\n", mask,
          (uint16_t) s_port.value);
#endif
  s_port.lmr = mask;
  if ((s_port.value & s_port.lmr & s_port.imr) && hal.limits.interrupt_callback)
  {
    hal.limits.interrupt_callback(limits_get_state());
  }
}

void vlimits_stepper_mask(stepper_t *stepper)
{
  uint16_t mask = 0;
  size_t pin = 1;
  uint8_t outmask;

  if (stepper == NULL)
  {
    // mask all limits
    vlimits_set_mask(0);
    return;
  }

  outmask = stepper->dir_outbits.value;

  for (size_t idx = 0; idx < N_AXIS; ++idx)
  {
    if (stepper->steps[idx])
    {
      if (pin & outmask)
        mask |= (pin);
      else
        mask |= (pin << VLIMIT_PIN_X_MAX);
    }
    pin <<= 1;
  }
  vlimits_set_mask(mask);
}

void vlimits_init()
{
  s_port.value = 0;
  s_port.imr = VLIMIT_PIN_MASK; // interrupts are not masked
  s_port.lmr = 0;               // limits are masked on startup

  s_pin_index = hal.port.num_digital_out;
  on_port = hal.port;

  hal.port.num_digital_out += AUX_OUT_SIZE;
  hal.port.digital_out = digital_out;
  hal.port.get_pin_info = get_pin_info;

  hal.limits.enable = limits_enable;
  hal.limits.get_state = limits_get_state;
  hal.homing.get_state = limits_get_state;

  on_enumerate_pins = hal.enumerate_pins;
  hal.enumerate_pins = enumerate_pins;
}
