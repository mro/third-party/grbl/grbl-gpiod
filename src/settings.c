/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-03-08
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#include "settings.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef HAVE_GETOPT_LONG
#include <getopt.h>
#elif _POSIX_C_SOURCE >= 2 || _XOPEN_SOURCE
#include <unistd.h>
#else
#error "Settings implementation needs GNU extensions (getopt_long or getopt)"
#endif

settings_data_t set;

static inline void usage(const char *progname)
{
  printf(
#ifdef HAVE_GETOPT_LONG
    "Usage: \n"
    "%s [options]\n"
    "  Options:\n"
    "   -i, --input <input>       file/pipe name to read commands from. "
    "   (default = stdin)\n"
    "   -o, --output <output>     file/pipe name to write the output to."
    "   (default = stdout)\n"
    "   -p, --priority <priority> real-time stepper thread priority.    "
    "   (default = 0 -> No Real-time)\n"
    "   -g, --gpio <config>       file name containing the mapping between\n"
    "                             the logical I/O pin and the physical GPIO\n"
    "                             line. The format is <pin>:<chip>/<line>."
    " (default = gpio.conf)\n"
    "   -h, --help                this help.\n",
#else
    "Usage: \n"
    "%s [options]\n"
    "  Options:\n"
    "   -i <input>    file/pipe name to read commands from. "
    "   (default = stdin)\n"
    "   -o <output>   file/pipe name to write the output to."
    "   (default = stdout)\n"
    "   -p <priority> real-time stepper thread priority.    "
    "   (default = 0 -> No Real-time)\n"
    "   -g <config>   file name containing the mapping between\n"
    "                 the logical I/O pin and the physical GPIO\n"
    "                 line. The format is <pin>:<chip>/<line>."
    " (default = gpio.conf)\n"
    "   -h            this help.\n",
#endif
    progname);
}

static inline int get_option(int argc, char *const argv[])
{
#ifdef HAVE_GETOPT_LONG
  static const struct option long_options[] = {
    {"help", no_argument, 0, 'h'},
    {"input", required_argument, 0, 'i'},
    {"output", required_argument, 0, 'o'},
    {"priority", required_argument, 0, 'p'},
    {"gpio", required_argument, 0, 'g'},
    {0, 0, 0, 0}};

  return getopt_long(argc, argv, "hi:o:p:g:", long_options, NULL);
#else
  return getopt(argc, argv, "hi:o:p:g:");
#endif
}

int settings_data_init(int argc, char *const argv[])
{
  int c;

  // defaults
  set.input_stream[0] = '\0';
  set.output_stream[0] = '\0';

  while ((c = get_option(argc, argv)) != -1)
  {
    switch (c)
    {
    case 'i':
      strncpy(set.input_stream, optarg, FILE_NAME_SIZE - 1);
      set.input_stream[FILE_NAME_SIZE - 1] = '\0';
      break;
    case 'o':
      strncpy(set.output_stream, optarg, FILE_NAME_SIZE - 1);
      set.output_stream[FILE_NAME_SIZE - 1] = '\0';
      break;
    case 'p':
      set.priority = atoi(optarg);
      set.input_stream[FILE_NAME_SIZE - 1] = '\0';
      break;
    case 'g':
      strncpy(set.gpio_config, optarg, FILE_NAME_SIZE - 1);
      set.gpio_config[FILE_NAME_SIZE - 1] = '\0';
      break;
    case '?':
    case 'h':
    default: usage(argv[0]); return -1;
    }
  }

  if (argc > optind)
  {
    printf("No arguments required.\n");
    usage(argv[0]);
    return -1;
  }

  return 0;
}
