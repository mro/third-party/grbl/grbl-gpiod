/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-21
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#ifndef REALTIME_THREAD_H
#define REALTIME_THREAD_H

#include <stdbool.h>
#include <stdint.h>

typedef struct rt_thread rt_thread_t;
typedef void (*rt_thread_function_t)(void);

/**
 * @brief initialize and start the real-time thread
 * @param routine function to be executed by the thread
 * @param priority scheduling priority ('>= 1' enables the real-time scheduling
 *                 policy, the normal scheduling one otherwise)
 * @param error pointer to the location where the eventual error message is
 *              copied, if not NULL
 * @returns pointer to thread data if successfully started, NULL otherwise
 */
rt_thread_t *rt_thread_start(rt_thread_function_t routine,
                             int priority,
                             char **error);

/**
 * @brief enable the routine execution and repeat it after each interval
 * @param thread pointer to thread data initialized with 'rt_thread_start'
 * @param interval time period in which to run the routine (in nanoseconds).
 *                 Setting a value '<= 0' the routine execution is disabled
 * @param accurate enable a much more precise time period at the cost of
 *                 higher CPU consumption if true
 * __NOTE__: the routine execution re-starts immediately if its runtime is
 *           longer than the duration of the interval
 */
void rt_thread_enable(rt_thread_t *thread, int32_t interval, bool accurate);

/**
 * @brief stop the real-time thread and release its resources
 * @param thread pointer to location of thread data to be freed
 * @param error pointer to the location where the eventual error message is
 *              copied, if not NULL
 * @returns true if the thread was stopped correctly, false otherwise
 */
bool rt_thread_stop(rt_thread_t **thread, char **error);

/**
 * @brief suspend/resume the real-time thread
 * @param thread pointer to thread data initialized with 'rt_thread_start'
 * @param suspend suspends if true, resume if false
 * @param error pointer to the location where the eventual error message is
 *              copied, if not NULL
 */
void rt_thread_suspend(rt_thread_t *thread, bool suspend, char **error);

#endif /* REALTIME_THREAD_H */
