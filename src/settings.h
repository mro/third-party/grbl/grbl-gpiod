/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-03-08
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
**
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include "local-config.h"

#define FILE_NAME_SIZE 128

typedef struct
{
  char input_stream[FILE_NAME_SIZE];
  char output_stream[FILE_NAME_SIZE];
  char gpio_config[FILE_NAME_SIZE];
  int priority;
} settings_data_t;

extern settings_data_t set;

int settings_data_init(int argc, char *const argv[]);

#endif /* SETTINGS_H */
