/**
 * Copyright (C) 2021 CERN
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Created on: 2022-04-08
 *     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
 *
 */

#include <cstdlib>
#include <memory>
#include <string>
#include <thread>

#include <cppunit/TestFixture.h>

#include "../deps/grbl/grbl.h"
#include "test_helpers.hpp"
#include "utils/grbl/GrblGpiod.hpp"

using namespace utils::grbl;

class TestGrblGpiod : public CppUnit::TestFixture
{
protected:
    std::unique_ptr<GrblGpiod> grblGpiod;

    CPPUNIT_TEST_SUITE(TestGrblGpiod);
    CPPUNIT_TEST(wrongCommand);
    CPPUNIT_TEST(recoverAfterError);
    CPPUNIT_TEST(motionDuration);
    CPPUNIT_TEST(resetByCommand);
    CPPUNIT_TEST(exitByCommand);
    CPPUNIT_TEST(exitClosingInput);
    CPPUNIT_TEST(pauseResume);
    CPPUNIT_TEST(reportPins);
    CPPUNIT_TEST(triggeredMotion);
    CPPUNIT_TEST(singleAxisMotion);
    CPPUNIT_TEST(predefinedPositions);
    CPPUNIT_TEST(relativeMotion);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp() override
    {
        grblGpiod = std::unique_ptr<GrblGpiod>(new GrblGpiod{});
    }

    void tearDown() override { grblGpiod.reset(); }

    void waitForPosition(Axis axis, double position)
    {
        const double delta = 0.001;
        ASSERT_MSG(
            to_string(axis) + "-axis position should be close to " +
                std::to_string(position),
            waitFor(
                [this, &axis, &position, &delta]() {
                    const double current = grblGpiod->getAxisPosition(axis);
                    return (current <= (position + delta)) &&
                        (current >= (position - delta));
                },
                10, std::chrono::milliseconds(500)));
    }

    void wrongCommand()
    {
        // Send a wrong command
        grblGpiod->sendCmd("$$$");
        ASSERT_MSG("The command should not exist",
                   grblGpiod->expect("^error.*$"));
    }

    void recoverAfterError()
    {
        // Send a wrong command
        grblGpiod->sendCmd("toto");
        ASSERT_MSG("The command should fail", grblGpiod->expect("^error.*$"));

        // Send a correct g-code command
        grblGpiod->sendCmd("X2");
        ASSERT_MSG("The command should succeed", grblGpiod->expect("^ok\\s*$"));
    }

    void motionDuration()
    {
        // Set X-axis speeds (60 mm/min) and check
        grblGpiod->sendCmd("$110=60.0");
        ASSERT_MSG("Failed to set X-axis speed", grblGpiod->expect("^ok\\s*$"));

        grblGpiod->sendCmd("$110");
        ASSERT_MSG("X-axis speed not properly set",
                   grblGpiod->expect("^\\$110=60\\.0.*"));

        // Check initial positions
        grblGpiod->write("?");
        ASSERT_MSG(
            "X-, Y-, and Z-position should be zero",
            grblGpiod->expect(".*Idle\\|MPos:0\\.000,0\\.000,0\\.000.*"));

        // Move along X-axis by 2 mm (~ 2 sec) and check
        grblGpiod->sendCmd("G0 X2");
        ASSERT_MSG("Failed to move X-axis", grblGpiod->expect("^ok\\s*$"));

        ASSERT_MSG("X-axis position should be greater than 1 mm",
                   waitFor(
                       [this]() {
                           return (grblGpiod->getAxisPosition(Axis::X) > 1.0);
                       },
                       10, std::chrono::milliseconds(250)));

        ASSERT_MSG("X-axis position should be equal to 2 mm",
                   waitFor(
                       [this]() {
                           // Send "?" to Grbl-sim
                           grblGpiod->write("?");
                           return grblGpiod->expect(
                               ".*Idle\\|MPos:2\\.000,0\\.000,0\\.000.*");
                       },
                       5, std::chrono::milliseconds(200)));
    }

    void resetByCommand()
    {
        // Send reset command
        grblGpiod->sendCmd(std::string({CMD_RESET}));
        ASSERT_MSG("Should reset grbl-gpiod", grblGpiod->expect("^GrblHAL.*$"));
    }

    void exitByCommand()
    {
        // Send exit command
        grblGpiod->sendCmd(std::string({CMD_EXIT}));

        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        ASSERT_MSG("Should terminate grbl-gpiod",
                   waitFor([this]() { return grblGpiod->exited(); }, 3,
                           std::chrono::milliseconds(250)));
    }

    void exitClosingInput()
    {
        grblGpiod->closeInput();

        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        ASSERT_MSG("Should terminate grbl-gpiod",
                   waitFor([this]() { return grblGpiod->exited(); }, 3,
                           std::chrono::milliseconds(250)));
    }

    void pauseResume()
    {
        grblGpiod->write("?");
        EQ(true, grblGpiod->expectState(RunState::Idle));

        // Set X-axis speeds (5 mm/sec) and check
        grblGpiod->sendCmd("G1 F300");
        ASSERT_MSG("Failed to set X-axis speed", grblGpiod->expect("^ok\\s*$"));

        // Move along X-axis by 3.5 mm and check
        grblGpiod->sendCmd("G1 X3.5");

        ASSERT_MSG("Expected motion along X-axis",
                   waitFor(
                       [this]() {
                           grblGpiod->write("?");
                           return grblGpiod->expectState(RunState::Run);
                       },
                       3, std::chrono::milliseconds(200)));

        // Pause motion
        grblGpiod->sendCmd("M64P1");
        ASSERT_MSG("Failed to set port 'Pause/Resume' to on",
                   grblGpiod->expect("^ok\\s*$"));

        // Get position
        const double pos = grblGpiod->getAxisPosition(Axis::X);
        EQ_MSG("X-axis position should be less then 3.5 mm", true, (pos < 3.5));

        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        // Get position (again) and verify X-position is still the same
        DBL_EQ_MSG("X-axis position should not change", pos,
                   grblGpiod->getAxisPosition(Axis::X), 0);

        // Resume motion
        grblGpiod->sendCmd("M65P1");
        ASSERT_MSG("Failed to set port 'Pause/Resume' to off",
                   grblGpiod->expect("^ok\\s*$"));

        // Verify X-axis arrived at final position
        waitForPosition(Axis::X, 3.5);

        ASSERT_MSG("Grbl not idle",
                   waitFor(
                       [this]() {
                           grblGpiod->write("?");
                           return grblGpiod->expectState(RunState::Idle);
                       },
                       3, std::chrono::milliseconds(200)));
    }

    void reportPins()
    {
        // Ask for pin list
        grblGpiod->sendCmd("$PINS");
        ASSERT_MSG("Wrong pin report",
                   (grblGpiod->expect("[PIN:0,Aux out 0,Hardware Trigger]") &&
                    grblGpiod->expect("[PIN:1,Aux out 1,Pause/Resume]")));
    }

    void triggeredMotion()
    {
        // Verify X-axis is in its initial position
        DBL_EQ(0.0, grblGpiod->getAxisPosition(Axis::X), 0);

        // Arm trigger with motion
        grblGpiod->sendCmd("M62P0 G1 F300 X5");
        ASSERT_MSG("Failed to arm trigger with motion",
                   grblGpiod->expect("^ok\\s*$"));

        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        DBL_EQ_MSG("X-axis position should not change", 0.0,
                   grblGpiod->getAxisPosition(Axis::X), 0);

        // Trigger motion
        grblGpiod->sendCmd("M65P0");
        ASSERT_MSG("Failed to trigger motion", grblGpiod->expect("^ok\\s*$"));

        // Verify X-axis is moving
        ASSERT_MSG(
            "X-axis should move",
            waitFor(
                [this]() { return (grblGpiod->getAxisPosition(Axis::X) > 0); },
                3, std::chrono::milliseconds(200)));
    }

    void axisMotion(Axis axis)
    {
        // Verify the axis is in its initial position
        DBL_EQ(0.0, grblGpiod->getAxisPosition(axis), 0);

        const std::string axisStr = to_string(axis);
        grblGpiod->sendCmd("G1 F400 " + axisStr + "1.5"); // F300 ~ 6.7 mm/sec
        ASSERT_MSG("Failed to send motion command",
                   grblGpiod->expect("^ok\\s*$"));

        waitForPosition(axis, 1.5);

        ASSERT_MSG("Grbl not idle",
                   waitFor(
                       [this]() {
                           grblGpiod->write("?");
                           return grblGpiod->expectState(RunState::Idle);
                       },
                       3, std::chrono::milliseconds(100)));
    }

    void singleAxisMotion()
    {
        for (size_t i = 0; i < N_AXIS; ++i)
        {
            axisMotion(static_cast<Axis>(i));
        }
    }

    void predefinedPositions()
    {
        struct coordinate
        {
            Axis axis;
            double value;
        };
        std::vector<coordinate> G28{N_AXIS}, G30{N_AXIS};
        std::ostringstream g28, g30;

        // init positions
        double val = 0.0;
        for (size_t i = 0; i < N_AXIS; ++i)
        {
            val += 0.1;
            G28[i].axis = G30[i].axis = static_cast<Axis>(i);
            G28[i].value = val;
            G30[i].value = val * 2;
            g28 << to_string(G28[i].axis) << G28[i].value;
            g30 << to_string(G30[i].axis) << G30[i].value;
        }

        // Move to position G28
        grblGpiod->sendCmd(g28.str());
        waitForPosition(G28[N_AXIS - 1].axis, G28[N_AXIS - 1].value);

        // store position
        grblGpiod->sendCmd("G28.1");

        // Move to position G30
        grblGpiod->sendCmd(g30.str());
        waitForPosition(G30[N_AXIS - 1].axis, G30[N_AXIS - 1].value);

        // store position
        grblGpiod->sendCmd("G30.1");

        // move to predefined position using related commands
        // move to G28
        grblGpiod->sendCmd("G28");
        waitForPosition(G28[N_AXIS - 1].axis, G28[N_AXIS - 1].value);

        // move to G30
        grblGpiod->sendCmd("G30");
        waitForPosition(G30[N_AXIS - 1].axis, G30[N_AXIS - 1].value);
    }

    void relativeMotion()
    {
        const Axis axis = Axis::X;
        const std::string axisStr = to_string(axis);

        // Verify the axis is in its initial position
        DBL_EQ(0.0, grblGpiod->getAxisPosition(axis), 0);

        grblGpiod->sendCmd("G1 F300 " + to_string(axis) +
                           "2.5"); // F300 = 5 mm/sec
        ASSERT_MSG("Failed to send motion command",
                   grblGpiod->expect("^ok\\s*$"));

        waitForPosition(axis, 2.5);

        // Enable incremental distance mode (default is absolute distance, i.e.
        // G90)
        grblGpiod->sendCmd("G91");

        // Move by 1.5 mm
        grblGpiod->sendCmd(to_string(axis) + "1.5");
        waitForPosition(axis, 4.0); // 2.5 + 1.5
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestGrblGpiod);
