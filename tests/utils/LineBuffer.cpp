/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-12T18:06:23+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#include "LineBuffer.hpp"

#include <algorithm>
#include <iostream>

using namespace utils;

LineBuffer::LineBuffer(std::size_t size) : m_count(0), m_pos(0), m_buffer(size)
{}

bool LineBuffer::add(char c)
{
    if (m_pos + 1 > m_buffer.size())
    {
        std::cerr << "[CRITICAL] {LineBuffer} buffer full, flushing"
                  << std::endl;
        m_pos = 0;
    }
    m_buffer[m_pos++] = c;
    if (c == '\n')
        ++m_count;
    return m_count != 0;
}

bool LineBuffer::fwd(std::size_t sz)
{
    if (m_pos + sz > m_buffer.size())
    {
        std::cerr << "[EMERG] {LineBuffer} buffer overflow" << std::endl;
        sz = m_buffer.size() - m_pos;
    }
    std::for_each(m_buffer.cbegin() + m_pos, m_buffer.cbegin() + m_pos + sz,
                  [this](char c) {
                      if (c == '\n')
                          ++m_count;
                  });
    m_pos += sz;
    return m_count != 0;
}

bool LineBuffer::takeLine(std::string &line)
{
    const std::vector<char>::const_iterator bend = m_buffer.cbegin() + m_pos;
    std::vector<char>::const_iterator it = std::find(m_buffer.cbegin(), bend,
                                                     '\n');
    if (it != bend)
    {
        line = std::string(m_buffer.cbegin(), it);
        m_pos -= line.size() + 1;
        std::copy(++it, bend, m_buffer.begin());
        --m_count;
        return true;
    }
    return false;
}
