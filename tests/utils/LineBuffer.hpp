/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-12T18:05:36+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@gmail.com>
**
*/

#ifndef LINEBUFFER_HPP__
#define LINEBUFFER_HPP__

#include <cstdint>
#include <string>
#include <vector>

namespace utils {

class LineBuffer
{
public:
    /**
     * @brief LineBuffer fixed-size line buffering utility
     * @param size max-buffered bytes size
     */
    explicit LineBuffer(std::size_t size = defaultSize);

    /**
     * @brief add a character in the buffer
     * @param[in] c character to inject
     * @return true if a line is available
     * @details on overflow buffer is flushed
     */
    bool add(char c);

    /**
     * @brief move buffer forward
     * @details this is used when direct-injecting data in the buffer
     * @param[in]  sz number of bytes to move to
     * @return true if a line is available
     */
    bool fwd(std::size_t sz);

    /**
     * @brief consume one line from the buffer
     * @param[out]  line consumed line
     * @return true on success
     */
    bool takeLine(std::string &line);

    /**
     * @brief check if lines are available
     * @return true if a line is available in buffer
     */
    inline bool hasLine() const { return m_count != 0; }

    /**
     * @brief clear buffer
     */
    inline void reset() { m_pos = m_count = 0; }

    /**
     * @return number of bytes free in buffer
     */
    inline std::size_t remaining() const { return m_buffer.size() - m_pos; }

    /**
     * @return pointer past the end of buffered data
     */
    inline char *end() { return &m_buffer[m_pos]; }

    static constexpr std::size_t defaultSize = 1024;

protected:
    std::size_t m_count;
    std::size_t m_pos;
    std::vector<char> m_buffer;
};

} // namespace utils

#endif
