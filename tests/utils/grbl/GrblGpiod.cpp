/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-11T17:16:38+01:00
**     Author: Michal Mysior <mmysior> <michal.mysior@cern.ch>
**
** Adapted by Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
*/

#include "GrblGpiod.hpp"

#include <algorithm>
#include <cerrno>
#include <cstring>
#include <exception>
#include <iostream>
#include <thread>

#include <fcntl.h>
#include <poll.h>
#include <regex.h>
#include <signal.h>
#include <spawn.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

#include "local-config.h"

using namespace utils::grbl;
using namespace std::chrono;

// Grbl-Gpiod defines for executable location
#define GRBL_GPIOD_PROGRAM_NAME (BUILDDIR "/src/grbl-gpiod")
#define GRBL_GPIOD_PROGRAM_EXE "grbl-gpiod"
static const ccut::Regex s_okResponse("^ok\\s*$");
static const ccut::Regex s_grblInit("^GrblHAL.*$");
static const std::string s_crnl("\r\n");

const milliseconds GrblGpiod::defaultExpectTimeout(500);

GrblGpiod::GrblGpiod()
{
    // Launch Grbl-Gpiod, set up pipes
    int pa2s[2];
    int ps2a[2];

    ::pipe(pa2s);
    ::pipe(ps2a);

    posix_spawn_file_actions_t fileActions;
    posix_spawn_file_actions_init(&fileActions);
    posix_spawn_file_actions_addclose(&fileActions, pa2s[1]);
    posix_spawn_file_actions_adddup2(&fileActions, pa2s[0], STDIN_FILENO);
    posix_spawn_file_actions_addclose(&fileActions, pa2s[0]);
    posix_spawn_file_actions_addclose(&fileActions, ps2a[0]);
    posix_spawn_file_actions_adddup2(&fileActions, ps2a[1], STDOUT_FILENO);
    posix_spawn_file_actions_addclose(&fileActions, ps2a[1]);

    char *args[] = {const_cast<char *>(GRBL_GPIOD_PROGRAM_EXE),
                    static_cast<char *>(NULL)};
    int err = posix_spawn(&m_simPid, GRBL_GPIOD_PROGRAM_NAME, &fileActions,
                          NULL, args, NULL);
    if (err != 0)
    {
        errno = err;
        std::cerr << "[CRITICAL] {GrblGpiod} failed to spawn grbl-gpiod : "
                  << strerror(errno) << std::endl;
    }

    posix_spawn_file_actions_destroy(&fileActions);

    ::close(pa2s[0]);
    m_out = pa2s[1];
    ::close(ps2a[1]);
    m_in = ps2a[0];

    ::pipe(m_wakeFd);
    ::fcntl(m_wakeFd[0], ::fcntl(m_wakeFd[0], F_GETFL, 0) | O_NONBLOCK);
    ::fcntl(m_wakeFd[1], ::fcntl(m_wakeFd[1], F_GETFL, 0) | O_NONBLOCK);
    ::fcntl(m_out, F_SETFL, ::fcntl(m_out, F_GETFL, 0) | O_NONBLOCK);
    ::fcntl(m_in, F_SETFL, ::fcntl(m_in, F_GETFL, 0) | O_NONBLOCK);

    if (!expect(s_grblInit, milliseconds(3000)))
        std::cerr << "[ERROR] {GrblGpiod} grbl banner not found" << std::endl;
    else
        std::cout << "[NOTICE] {GrblGpiod} GrblSim ready" << std::endl;
}

GrblGpiod::~GrblGpiod()
{
    // Kill Grbl-Gpiod
    closeInput();
    ::close(m_in);
    ::close(m_wakeFd[0]);
    ::close(m_wakeFd[1]);

    int status;
    std::this_thread::sleep_for(milliseconds(100));
    if (::waitpid(m_simPid, &status, WNOHANG) <= 0)
    {
        ::kill(m_simPid, SIGKILL);
        ::waitpid(m_simPid, &status, 0);
    }
}

bool GrblGpiod::expect(const ccut::Regex &regex,
                       std::string &line,
                       const milliseconds &timeout)
{
    const steady_clock::time_point deadline = steady_clock::now() + timeout;

    while (steady_clock::now() <= deadline)
    {
        steady_clock::time_point now = steady_clock::now();
        const milliseconds remaining = (now < deadline) ?
            duration_cast<milliseconds>(deadline - now) :
            milliseconds::zero();

        if (!m_buffer.hasLine())
        {
            try
            {
                wait(remaining);
            }
            catch (const std::exception &ex)
            {
                std::cerr << "[ERROR] {GrblGpiod} " << ex.what() << std::endl;
                return false;
            }
            read();
        }

        while (m_buffer.takeLine(line))
        {
            if (regex.search(line))
            {
                std::cout << "[DEBUG] {GrblGpiod} <-- " << line << std::endl;
                return true;
            }
            else
            {
                std::cout << "[DEBUG] {GrblGpiod} x<- " << line << std::endl;
            }
        }
    }
    line.clear();
    return false;
}

double GrblGpiod::getAxisPosition(const Axis axis)
{
    const std::string valuePlaceholder("(-?[0-9]+\\.[0-9]*)");
    const size_t axisIndex = static_cast<size_t>(axis);
    std::string posRegexStr("[MW]Pos:");
    std::string line;

    posRegexStr += valuePlaceholder;
    for (size_t i = 1; i <= axisIndex; ++i)
    {
        posRegexStr += ("," + valuePlaceholder);
    }
    ccut::Regex posRegex(posRegexStr);

    // get position
    write("?");

    if (expect(posRegex, line))
    {
        std::vector<std::string> pos;
        posRegex.match(line, pos);
        if (pos.size() == axisIndex + 2)
        {
            return atof(pos[axisIndex + 1].c_str());
        }
    }

    std::cerr << "[ERROR] {GrblGpiod} " << to_string(axis)
              << "-position not found" << std::endl;
    return 0;
}

void GrblGpiod::flush()
{
    m_buffer.reset();
}

void GrblGpiod::sendCmd(const std::string &command)
{
    std::cout << "[DEBUG] {GrblGpiod} --> " << command << std::endl;
    write(command);
    write(s_crnl);
}

void GrblGpiod::read()
{
    if (!m_buffer.remaining())
    {
        std::cerr << "[ERROR] {GrblGpiod} flushing buffer" << std::endl;
        m_buffer.reset();
    }

    ssize_t sz = ::read(m_in, m_buffer.end(), m_buffer.remaining());
    if (sz > 0)
    {
        m_buffer.fwd(sz);
    }
}

void GrblGpiod::write(const std::string &raw)
{
    for (size_t pos = 0; pos < raw.size();)
    {
        ssize_t ret = ::write(m_out, raw.c_str() + pos, raw.size() - pos);
        if (ret < 0)
        {
            std::cerr << "[ERROR] {GrblGpiod} "
                      << "failed to send command : " << strerror(errno)
                      << std::endl;
            return;
        }
        pos += ret;
        if (pos != raw.size())
            std::this_thread::sleep_for(milliseconds(10));
    }
}

bool GrblGpiod::wait(const milliseconds &timeout)
{
    struct pollfd pfds[2] = {{m_in, POLLIN | POLLERR, 0},
                             {m_wakeFd[0], POLLIN | POLLERR, 0}};
    if (::poll(pfds, 2, timeout.count() + 1) >= 0)
    {
        if (pfds[1].revents & POLLIN)
        {
            std::vector<char> buffer(10);
            ::read(m_wakeFd[0], buffer.data(), buffer.size());
        }
        if (pfds[0].revents & POLLERR || pfds[1].revents & POLLERR)
            throw std::runtime_error("Input/Output error");

        return (pfds[0].revents & POLLIN);
    }
    else
        throw std::runtime_error("Input/Output error");
    return false;
}

void GrblGpiod::wake()
{
    ::write(m_wakeFd[1], "X", 1);
}

bool GrblGpiod::exited()
{
    int status;
    if (::waitpid(m_simPid, &status, WNOHANG) > 0)
        return WIFEXITED(status);
    else
        return false;
}

void GrblGpiod::closeInput()
{
    char stopChar = 0x06;

    if (m_out > 0)
    {
        // ignore SIGPIPE signal when trying to send stop char, as the reading
        // end of pa2s pipe could be already closed internally through CMD_EXIT
        signal(SIGPIPE, SIG_IGN);

        ::write(m_out, &stopChar, 1);
        ::close(m_out);
        m_out = -1;
    }
}
