/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-16T08:50:17+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "GrblTypes.hpp"

#include <stdexcept>

namespace utils {

namespace grbl {

static const std::map<std::string, RunState> s_runStateMap = {
    {"Idle", RunState::Idle},   {"Run", RunState::Run},
    {"Hold", RunState::Hold},   {"Jog", RunState::Jog},
    {"Home", RunState::Home},   {"Alarm", RunState::Alarm},
    {"Check", RunState::Check}, {"Door", RunState::Door},
    {"Sleep", RunState::Sleep}, {"Tool", RunState::Tool}};

std::string to_string(Axis axis)
{
    switch (axis)
    {
    case Axis::X: return "X";
    case Axis::Y: return "Y";
    case Axis::Z: return "Z";
    case Axis::A: return "A";
    case Axis::B: return "B";
    case Axis::C: return "C";
    case Axis::U: return "U";
    case Axis::V: return "V";
    default: return "Unknown";
    }
}

template<>
Axis from_string(const std::string &str)
{
    if (str.empty())
        throw std::invalid_argument("invalid Axis (empty)");

    switch (str[0])
    {
    case 'X': return Axis::X;
    case 'Y': return Axis::Y;
    case 'Z': return Axis::Z;
    case 'A': return Axis::A;
    case 'B': return Axis::B;
    case 'C': return Axis::C;
    case 'U': return Axis::U;
    case 'V': return Axis::V;
    default: throw std::invalid_argument("invalid Axis " + str);
    }
}

std::string to_string(RunState state)
{
    switch (state)
    {
    case RunState::Idle: return "Idle";
    case RunState::Run: return "Run";
    case RunState::Hold: return "Hold";
    case RunState::Jog: return "Jog";
    case RunState::Home: return "Home";
    case RunState::Alarm: return "Alarm";
    case RunState::Check: return "Check";
    case RunState::Door: return "Door";
    case RunState::Sleep: return "Sleep";
    case RunState::Tool: return "Tool";
    default: return "Unknown";
    }
}

template<>
RunState from_string(const std::string &str)
{
    decltype(s_runStateMap)::const_iterator it = s_runStateMap.find(str);
    if (it == s_runStateMap.cend())
        return RunState::Unknown;
    else
        return it->second;
}

} // namespace grbl

} // namespace utils
