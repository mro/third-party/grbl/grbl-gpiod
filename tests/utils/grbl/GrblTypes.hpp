/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-14T18:50:21+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef GRBL_TYPES_HPP__
#define GRBL_TYPES_HPP__

#include <cstdint>
#include <map>
#include <string>

namespace utils {

namespace grbl {
/**
 * @addtogroup grbl
 * @{
 */

template<typename T>
T from_string(const std::string &line);

typedef float grbl_float_t;

enum class Axis
{
    X = 0,
    Y = 1,
    Z = 2,
    A = 3,
    B = 4,
    C = 5,
    U = 6,
    V = 7
};
std::string to_string(Axis axis);

template<>
Axis from_string(const std::string &line);

enum class RunState
{
    Idle,
    Run,
    Hold,
    Jog,
    Home,
    Alarm,
    Check,
    Door,
    Sleep,
    Tool,
    Unknown
};
std::string to_string(RunState runState);

template<>
RunState from_string(const std::string &line);

/** @} grbl */

} // namespace grbl

} // namespace utils

#endif
