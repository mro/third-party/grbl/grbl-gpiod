/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-02-11T16:32:53+01:00
*      Author: Michal Mysior <mmysior> <michal.mysior@cern.ch>
**
*/

#ifndef GRBL_GPIOD_HPP__
#define GRBL_GPIOD_HPP__

#include <chrono>
#include <cstdint>
#include <string>
#include <vector>

#include "../LineBuffer.hpp"
#include "../Regex.hpp"
#include "GrblTypes.hpp"

namespace utils {

namespace grbl {

class GrblGpiod
{
public:
    GrblGpiod();
    ~GrblGpiod();

    bool expect(const std::string &regex,
                const std::chrono::milliseconds &timeout = defaultExpectTimeout)
    {
        std::string tmp;
        return expect(regex, tmp, timeout);
    }

    bool expect(const std::string &regex,
                std::string &line,
                const std::chrono::milliseconds &timeout = defaultExpectTimeout)
    {
        return expect(ccut::Regex(regex), line, timeout);
    }

    bool expect(const ccut::Regex &regex,
                const std::chrono::milliseconds &timeout = defaultExpectTimeout)
    {
        std::string tmp;
        return expect(regex, tmp, timeout);
    }

    bool expect(const ccut::Regex &regex,
                std::string &line,
                const std::chrono::milliseconds &timeout = defaultExpectTimeout);

    bool expectState(
        const RunState state,
        const std::chrono::milliseconds &timeout = defaultExpectTimeout)
    {
        return expect(to_string(state), timeout);
    }

    void flush();

    double getAxisPosition(const Axis axis);

    void read();
    void write(const std::string &data);
    void sendCmd(const std::string &command);
    bool wait(const std::chrono::milliseconds &timeout);
    void wake();
    bool exited();
    void closeInput();

    static const std::chrono::milliseconds defaultExpectTimeout;

protected:
    int m_in;
    int m_out;
    int m_wakeFd[2];
    pid_t m_simPid;

    utils::LineBuffer m_buffer;
};

} // namespace grbl

} // namespace utils

#endif
