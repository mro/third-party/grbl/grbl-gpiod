/**
 * Copyright (C) 2021 CERN
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Created on: 2022-04-08
 *     Author: Gabriele De Blasi <gdeblasi> <gabriele.de.blasi@cern.ch>
 *
 */

#include <cstdlib>
#include <memory>
#include <string>
#include <thread>

#include <cppunit/TestFixture.h>

#include "../deps/grbl/grbl.h"
#include "test_helpers.hpp"
#include "utils/grbl/GrblGpiod.hpp"

using namespace utils::grbl;

class TestHolding : public CppUnit::TestFixture
{
protected:
    std::unique_ptr<GrblGpiod> grblGpiod;

    CPPUNIT_TEST_SUITE(TestHolding);
    CPPUNIT_TEST(holdResumeCommands);
    CPPUNIT_TEST(readSettings);
    CPPUNIT_TEST(handleLimits);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp() override
    {
        grblGpiod = std::unique_ptr<GrblGpiod>(new GrblGpiod{});
    }

    void tearDown() override { grblGpiod.reset(); }

    void waitForPosition(Axis axis, double position)
    {
        const double delta = 0.001;
        ASSERT_MSG(
            to_string(axis) + "-axis position should be close to " +
                std::to_string(position),
            waitFor(
                [this, &axis, &position, &delta]() {
                    const double current = grblGpiod->getAxisPosition(axis);
                    return (current <= (position + delta)) &&
                        (current >= (position - delta));
                },
                10, std::chrono::milliseconds(500)));
    }

    void holdResumeCommands()
    {
        const Axis axis = Axis::Y;
        const std::string axisStr = to_string(axis);
        // Verify the axis is in its initial position
        DBL_EQ(0.0, grblGpiod->getAxisPosition(axis), 0);

        // Move along axis
        grblGpiod->sendCmd("G1 F100 " + axisStr + "2.0");

        ASSERT_MSG("Motion along axis should be started",
                   waitFor(
                       [this]() {
                           grblGpiod->write("?");
                           return grblGpiod->expectState(RunState::Run);
                       },
                       3, std::chrono::milliseconds(200)));

        grblGpiod->write("!"); // FEED HOLD command (SUSPEND)

        ASSERT_MSG("Motion along " + axisStr + "-axis should be suspended",
                   waitFor(
                       [this]() {
                           grblGpiod->write("?");
                           // Hold:0 -> hold complete. Ready to resume
                           // Hold:1 -> hold in-progress. Reset will throw an
                           // alarm
                           return grblGpiod->expect("Hold:0");
                       },
                       10, std::chrono::milliseconds(250)));

        // Get position
        const double pos = grblGpiod->getAxisPosition(axis);

        std::this_thread::sleep_for(std::chrono::milliseconds(200));

        // Get position (again) and verify if position is still the same
        DBL_EQ_MSG("Position should not change", pos,
                   grblGpiod->getAxisPosition(axis), pos);

        grblGpiod->write("~"); // CYCLE START command (RESUME)

        waitForPosition(axis, 2.0);
    }

    void readSettings()
    {
        grblGpiod->write("?");
        EQ_MSG("System should be in IDLE state", true,
               grblGpiod->expectState(RunState::Idle));

        grblGpiod->sendCmd("$110=123"); // set setting
        ASSERT_MSG("Failed to get setting", grblGpiod->expect("ok"));

        grblGpiod->write("!"); // FEED HOLD command (SUSPEND)
        ASSERT_MSG("System should be in HOLD state",
                   waitFor(
                       [this]() {
                           grblGpiod->write("?");
                           return grblGpiod->expectState(RunState::Hold);
                       },
                       10, std::chrono::milliseconds(250)));

        grblGpiod->sendCmd("$110"); // read settings is allowed
        ASSERT_MSG("Reading settings during hold state should be permitted",
                   grblGpiod->expect("\\$110=123"));

        grblGpiod->sendCmd("$110=50"); // write settings is prohibited
        ASSERT_MSG("Writing settings during hold state should be prohibited",
                   grblGpiod->expect("error"));
    }

    void handleLimits()
    {
        grblGpiod->sendCmd("$10=1043"); // alarmSubState=0x400 (+basic stuff)
        grblGpiod->sendCmd("$21=1");    // enable hard limits
        ASSERT_MSG("Failed to enable hard-limits", grblGpiod->expect("ok"));

        // Verify the axis is in its initial position
        DBL_EQ(0.0, grblGpiod->getAxisPosition(Axis::X), 0);

        // GPIO Activation
        {
            // Move along axis X
            grblGpiod->sendCmd("G1 F100 X2");

            ASSERT_MSG("Motion along X-axis should be started",
                       waitFor(
                           [this]() {
                               grblGpiod->write("?");
                               return grblGpiod->expectState(RunState::Run);
                           },
                           3, std::chrono::milliseconds(200)));

            grblGpiod->write("!"); // FEED HOLD command (SUSPEND)
            ASSERT_MSG("Motion along X-axis should be suspended",
                       waitFor(
                           [this]() {
                               grblGpiod->write("?");
                               return grblGpiod->expect("Hold:0");
                           },
                           10, std::chrono::milliseconds(250)));

            grblGpiod->sendCmd("M64P10"); // Enable X-Max limit
            ASSERT_MSG("GPIO activation during hold state should be permitted",
                       grblGpiod->expect("ok"));

            grblGpiod->write("~"); // CYCLE START command (RESUME)

            ASSERT_MSG("Hard-limit on X-axis should be triggered",
                       waitFor(
                           [this]() {
                               grblGpiod->write("?");
                               return grblGpiod->expect("Alarm:1");
                           },
                           10, std::chrono::milliseconds(250)));
        }

        // Reset grbl
        grblGpiod->write("\x18");
        ASSERT_MSG("Grbl not reset", grblGpiod->expect("unlock"));
        grblGpiod->sendCmd("$X");
        ASSERT_MSG("Failed to unlock", grblGpiod->expect("Unlocked"));
        grblGpiod->write("?");
        EQ_MSG("System should be in IDLE state", true,
               grblGpiod->expectState(RunState::Idle));

        // GPIO Deactivation
        {
            grblGpiod->write("!"); // FEED HOLD command (SUSPEND)
            ASSERT_MSG("System should be in HOLD state",
                       waitFor(
                           [this]() {
                               grblGpiod->write("?");
                               return grblGpiod->expectState(RunState::Hold);
                           },
                           10, std::chrono::milliseconds(250)));

            grblGpiod->sendCmd("M65P10"); // Disable X-Max limit
            ASSERT_MSG(
                "GPIO deactivation during hold state should be permitted",
                grblGpiod->expect("ok"));

            grblGpiod->write("~"); // CYCLE START command (RESUME)
            ASSERT_MSG("Failed to resume",
                       waitFor(
                           [this]() {
                               grblGpiod->write("?");
                               return grblGpiod->expectState(RunState::Idle);
                           },
                           10, std::chrono::milliseconds(250)));

            // Restart motion along axis X
            grblGpiod->sendCmd("G1 F100 X2");

            waitForPosition(Axis::X, 2.0);
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestHolding);
