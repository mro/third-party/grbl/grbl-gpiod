/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-06-20T16:22:26
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <vector>

#include <cppunit/TestFixture.h>

#include "grbl/config.h"
#include "local-config.h"
#include "test_helpers.hpp"
#include "utils/Regex.hpp"
#include "utils/grbl/GrblGpiod.hpp"

using namespace utils::grbl;

static std::map<std::string, size_t> settingIds = {};

class TestResolvers : public CppUnit::TestFixture
{
protected:
    std::unique_ptr<GrblGpiod> grbl;

    CPPUNIT_TEST_SUITE(TestResolvers);
    CPPUNIT_TEST(settings);
    CPPUNIT_TEST(setSettings);
    CPPUNIT_TEST(driverPosition);
    CPPUNIT_TEST_SUITE_END();

    size_t getResolverSettingId(Axis axis, const std::string &name)
    {
        return getSettingId(to_string(axis) + "-axis " + name);
    }

    size_t getSettingId(const std::string &name)
    {
        if (settingIds.count(name) != 0)
            return settingIds[name];

        std::string line;
        grbl->sendCmd("$ES");
        ccut::Regex regex("\\[SETTING:([0-9]+)\\|.*\\|" + name);
        std::vector<std::string> out{2};

        ASSERT_MSG("Failed to find setting: " + name,
                   waitFor([this, &regex,
                            &line]() { return grbl->expect(regex, line); },
                           3, std::chrono::milliseconds(100)));
        // wait for the settings to be printed out completely
        ASSERT_MSG("End of settings not found",
                   waitFor([this]() { return grbl->expect("ok"); }, 5,
                           std::chrono::milliseconds(200)));

        regex.match(line, out);
        size_t id = settingIds[name] = std::stol(out[1]);
        return id;
    }

    std::string getSetting(size_t id)
    {
        std::string line;
        ccut::Regex regex("\\$" + std::to_string(id) + "=(.*)[\r|\n]");

        grbl->sendCmd("$" + std::to_string(id));
        ASSERT_MSG("Failed to get setting:",
                   waitFor(
                       [this, &regex, &line]() {
                           return (grbl->expect(regex, line) &&
                                   grbl->expect("ok"));
                       },
                       3, std::chrono::milliseconds(100)));

        std::vector<std::string> out{2};
        regex.match(line, out);
        return out[1];
    }

public:
    void setUp() override
    {
        grbl = std::unique_ptr<GrblGpiod>(new GrblGpiod{});
    }

    void tearDown() override { grbl.reset(); }

    void settings()
    {
        std::vector<std::string> names = {
            "resolver enable",
            "step-lost",
            "resolver step-lost threshold",
            "resolver mode",
            "resolver position",
            "driver position",
        };
        for (const std::string &n : names)
        {
            getResolverSettingId(Axis::X, n);
        }
    }

    void setSettings()
    {
        struct setting
        {
            std::string name;
            std::string value;
        };
        std::vector<setting> settings = {{"resolver position", "1234"},
                                         {"resolver position", "12"},
                                         {"resolver mode", "1234"},
                                         {"resolver mode", "100"},
                                         {"resolver mode", "400"},
                                         {"driver position", "1234"},
                                         {"driver position", "0"},
                                         {"resolver step-lost", "1"},
                                         {"resolver step-lost", "0"},
                                         {"resolver step-lost threshold", "10"},
                                         {"resolver enable", "1"},
                                         {"resolver enable", "0"}

        };

        for (const setting &s : settings)
        {
            size_t setting_id = getResolverSettingId(Axis::X, s.name);

            grbl->sendCmd("$" + std::to_string(setting_id) + "=" + s.value);
            ASSERT_MSG("Failed to set " + s.name, grbl->expect("ok"));
            ASSERT_MSG("setting not updated", getSetting(setting_id) == s.value);
        }
    }

    void driverPosition()
    {
        size_t setting_id = getResolverSettingId(Axis::X, "driver position");
        size_t resolution_id = getSettingId("X-axis travel resolution");

        grbl->sendCmd("$" + std::to_string(setting_id) + "=100");
        ASSERT_MSG("Failed to set driver position", grbl->expect("ok"));

        double resolution = std::stod(getSetting(resolution_id));
        double position = grbl->getAxisPosition(Axis::X);
        DBL_EQ(100 / resolution, position, 0.001);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestResolvers);
