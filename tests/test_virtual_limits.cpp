/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-10-05T17:20:51
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <map>

#include <cppunit/TestFixture.h>

#include "grbl/config.h"
#include "local-config.h"
#include "test_helpers.hpp"
#include "utils/Regex.hpp"
#include "utils/grbl/GrblGpiod.hpp"

using namespace utils::grbl;

static std::map<Axis, std::string> minPins;
static std::map<Axis, std::string> maxPins;

class TestVirtualLimits : public CppUnit::TestFixture
{
protected:
    std::unique_ptr<GrblGpiod> grbl;

    CPPUNIT_TEST_SUITE(TestVirtualLimits);
    CPPUNIT_TEST(loadPins);
    CPPUNIT_TEST(limitDetect);
    CPPUNIT_TEST(limitDisabled);
    CPPUNIT_TEST(handlePinsInAlarm);
    CPPUNIT_TEST(readSettingsInAlarm);
    CPPUNIT_TEST_SUITE_END();

    std::string getLimitPin(Axis axis, int direction)
    {
        std::string line;
        const std::string directionStr = ((direction > 0) ? "Max" : "Min");
        grbl->sendCmd("$PINS");
        ccut::Regex regex("\\[PIN:([0-9]+),.*," + to_string(axis) +
                          "-axis Limit Pin " + directionStr);
        ASSERT_MSG("Failed to find limit pin", grbl->expect(regex, line));

        std::vector<std::string> out(2);
        ASSERT_MSG("Failed to extract limit pin", regex.match(line, out));

        ASSERT_MSG("Failed to find $LIMITS ok", grbl->expect("ok"));
        return out[1];
    }

    void waitForPosition(Axis axis, double position)
    {
        ASSERT_MSG("Expected " + to_string(axis) + " position equal to " +
                       std::to_string(position),
                   waitFor(
                       [this, &axis, &position]() {
                           const double current = grbl->getAxisPosition(axis);
                           return (current <= (position + 0.001)) &&
                               (current >= (position - 0.001));
                       },
                       10, std::chrono::milliseconds(500)));
    }

    void resetGrbl()
    {
        grbl->write("\x18");
        ASSERT_MSG("Grbl not reset", grbl->expect("unlock"));
        grbl->sendCmd("$X");
        ASSERT_MSG("Failed to unlock", grbl->expect("Unlocked"));
        ASSERT_MSG("Failed to unlock", grbl->expect("ok"));
        grbl->write("\x18");
        ASSERT_MSG("Grbl not reset", grbl->expect("Grbl"));
    }

public:
    void setUp() override
    {
        grbl = std::unique_ptr<GrblGpiod>(new GrblGpiod{});
    }

    void tearDown() override { grbl.reset(); }

    void loadPins()
    {
        std::string line;
        ccut::Regex regex("\\[PIN:([0-9]+),.*,(.)-axis Limit Pin (...)]");
        std::vector<std::string> out(4);

        minPins.clear();
        maxPins.clear();

        grbl->sendCmd("$PINS");

        while (minPins.size() != N_AXIS || maxPins.size() != N_AXIS)
        {
            ASSERT_MSG("Failed to find limit pin", grbl->expect(regex, line));
            regex.match(line, out);
            if (out[3] == "Max")
                maxPins[from_string<Axis>(out[2])] = out[1];
            else
                minPins[from_string<Axis>(out[2])] = out[1];
        }
        ASSERT_MSG("Failed to find $LIMITS ok", grbl->expect("ok"));
    }

    void limitDetect(Axis axis)
    {
        loadPins();

        grbl->sendCmd("$10=1043"); // alarmSubState=0x400 (+basic stuff)
        ASSERT_MSG("Failed to configure report bits", grbl->expect("ok"));

        grbl->sendCmd("$21=1");
        ASSERT_MSG("Failed to enable hardware-limits", grbl->expect("ok"));
        grbl->sendCmd("M64P" + minPins[axis]);
        ASSERT_MSG("Failed to enable limit pin", grbl->expect("ok"));

        {
            // XMin is masked when moving towards XMax
            grbl->sendCmd(to_string(axis) + "2");
            ASSERT_MSG("Failed to send command", grbl->expect("ok"));
            waitForPosition(axis, 2.0);

            grbl->sendCmd(to_string(axis) + "0"); // trigger "min" hard limit
            ASSERT_MSG("Alarm not visible",
                       waitFor(
                           [this]() {
                               grbl->write("?");
                               return grbl->expect("Alarm:1");
                           },
                           3, std::chrono::milliseconds(200)));
            resetGrbl();
        }

        grbl->write("?");
        ASSERT_MSG("Grbl not idle", grbl->expectState(RunState::Idle));

        {
            grbl->sendCmd("M65P" + minPins[axis]);
            ASSERT_MSG("Failed to disable limit pin", grbl->expect("ok"));
            grbl->sendCmd("M64P" + maxPins[axis]);
            ASSERT_MSG("Failed to enable limit pin", grbl->expect("ok"));

            grbl->sendCmd(to_string(axis) + "1");
            ASSERT_MSG("Failed to send command", grbl->expect("ok"));
            waitForPosition(axis, 1.0);

            grbl->sendCmd(to_string(axis) + "3"); // trigger "max" hard limit
            ASSERT_MSG("Alarm not visible",
                       waitFor(
                           [this]() {
                               grbl->write("?");
                               return grbl->expect("Alarm:1");
                           },
                           3, std::chrono::milliseconds(200)));
            resetGrbl();
            grbl->sendCmd("M65P" + maxPins[axis]);
            ASSERT_MSG("Failed to disable limit pin", grbl->expect("ok"));
        }
    }

    void limitDetect()
    {
        for (size_t i = 0; i < N_AXIS; ++i)
        {
            limitDetect(static_cast<Axis>(i));
        }
    }

    void limitDisabled()
    {
        loadPins();
        grbl->sendCmd("$10=1043"); // alarmSubState=0x400 (+basic stuff)
        ASSERT_MSG("Failed to configure report bits", grbl->expect("ok"));

        grbl->sendCmd("$21=0");
        ASSERT_MSG("Failed to disable hardware-limits", grbl->expect("ok"));

        for (size_t i = 0; i < N_AXIS; ++i)
        {
            Axis axis = static_cast<Axis>(i);

            grbl->sendCmd("M64P" + minPins[axis]);
            ASSERT_MSG("Failed to enable limit pin", grbl->expect("ok"));
            grbl->sendCmd("M64P" + maxPins[axis]);
            ASSERT_MSG("Failed to enable limit pin", grbl->expect("ok"));
        }

        for (size_t i = 0; i < N_AXIS; ++i)
        {
            Axis axis = static_cast<Axis>(i);
            grbl->sendCmd(to_string(axis) + "2");
            ASSERT_MSG("Failed to send command", grbl->expect("ok"));
            waitForPosition(axis, 2.0);

            grbl->sendCmd(to_string(axis) + "0");
            ASSERT_MSG("Failed to send command", grbl->expect("ok"));
            waitForPosition(axis, 0.0);
        }
        ASSERT_MSG("Grbl not idle",
                   waitFor(
                       [this]() {
                           grbl->write("?");
                           return grbl->expectState(RunState::Idle);
                       },
                       3, std::chrono::milliseconds(100)));

        grbl->sendCmd("$21=1");
        ASSERT_MSG("Failed to enable hardware-limits", grbl->expect("ok"));

        grbl->sendCmd(to_string(Axis::X) +
                      "1"); /* any movement should trigger */
        ASSERT_MSG("Alarm not visible",
                   waitFor(
                       [this]() {
                           grbl->write("?");
                           return grbl->expect("Alarm:1");
                       },
                       3, std::chrono::milliseconds(200)));
        resetGrbl();
    }

    void handlePinsInAlarm()
    {
        const Axis axis = Axis::X;
        loadPins();

        grbl->sendCmd("$10=1043"); // alarmSubState=0x400 (+basic stuff)
        grbl->sendCmd("$21=1");
        ASSERT_MSG("Failed to enable hardware-limits", grbl->expect("ok"));

        grbl->sendCmd("M64P" + minPins[axis]); // enable min limit pin
        ASSERT_MSG("Failed to enable " + to_string(axis) + "-Min limit pin",
                   grbl->expect("ok"));

        // trigger min hard limit
        grbl->sendCmd(to_string(axis) + "-1");
        ASSERT_MSG("Min hard limit should be triggered",
                   waitFor(
                       [this]() {
                           grbl->write("?");
                           return grbl->expect("Alarm:1");
                       },
                       3, std::chrono::milliseconds(200)));

        // modify pins while in ALARM state
        grbl->sendCmd("M65P" + minPins[axis]); // disable min limit pin
        ASSERT_MSG(
            "Hard limit deactivation while in Alarm state should be permitted",
            grbl->expect("ok"));

        grbl->sendCmd("M64P" + maxPins[axis]); // enable max limit pin
        ASSERT_MSG(
            "Hard limit activation while in Alarm state should be permitted",
            grbl->expect("ok"));

        resetGrbl();

        // ensure min limit pin is disabled (restart motion)
        grbl->sendCmd(to_string(axis) + "-1");
        waitForPosition(axis, -1);

        // trigger max hard limit
        grbl->sendCmd(to_string(axis) + "1");
        ASSERT_MSG("Max hard limit should be triggered",
                   waitFor(
                       [this]() {
                           grbl->write("?");
                           return grbl->expect("Alarm:1");
                       },
                       3, std::chrono::milliseconds(200)));
    }

    void readSettingsInAlarm()
    {
        const Axis axis = Axis::X;
        loadPins();

        grbl->sendCmd("$10=1043"); // alarmSubState=0x400 (+basic stuff)

        grbl->sendCmd("$21=1");
        ASSERT_MSG("Failed to enable hardware-limits", grbl->expect("ok"));

        grbl->sendCmd("M64P" + minPins[axis]); // enable min limit pin
        ASSERT_MSG("Failed to enable " + to_string(axis) + "-Min limit pin",
                   grbl->expect("ok"));

        // trigger min hard limit
        grbl->sendCmd(to_string(axis) + "-1");
        ASSERT_MSG("Min hard limit should be triggered",
                   waitFor(
                       [this]() {
                           grbl->write("?");
                           return grbl->expect("Alarm:1");
                       },
                       3, std::chrono::milliseconds(200)));

        // read settings
        grbl->sendCmd("$21");
        ASSERT_MSG("Reading settings during alarm state should be permitted",
                   grbl->expect("\\$21=1"));

        // write settings
        grbl->sendCmd("$21=0");
        ASSERT_MSG("Writing settings during alarm state should be prohibited",
                   grbl->expect("error"));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestVirtualLimits);
