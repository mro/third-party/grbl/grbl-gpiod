# Step period jitter

The `nanosleep` and `clock_nanosleep` syscalls do not guarantee that the calling thread sleeps for no longer than a specified interval, leading to a variation of the end of the step period.
On the other hand, actively waiting for the end of an interval could prevent another thread from making better use of the CPU, especially in single-core ones.
Therefore, the solution used to achieve a more accurate period length is basically to sleep for the duration of the interval minus the jitter time and then check with a _busy wait_ that the end of the period has been reached.

Concerning jitter, it can either be preliminarily determined during a calibration phase or dynamically during the step generation, or both. The latter case should bring greater benefits since there is no need to wait for a certain number of steps before deriving the maximum jitter and because it can be adjusted over time.

Below is a histogram showing how jitter behaves in the different cases:

<div align="center">
  <img alt="step period jitter histogram" src="StepPeriodJitter_histogram.jpg">
</div>


The chart reveals that simply relying on the system call `clock_nanosleep`, the jitter is distributed over a wide range exceeding 240 microseconds (the maximum jitter is around 6.3 milliseconds).
Conversely, using any of the variants that consider the jitter time, this is mostly localised in the range between 10 and 80 microseconds. These have a very similar pattern, the only difference is that the frequency distribution for the variant with pre-calibration and dynamic calibration (auto-adjustment) converges to zero slightly earlier than the others (max. jitter approximately 218 microseconds).
