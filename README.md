# GRBL GPIOD

[GrblHAL](https://github.com/grblHAL/core) driver that can :
- use [libgpiod](https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/) as a back-end Gpiod-based implementation.
- run with virtual IOs.

**Note:** Forked from [GrblSim](https://github.com/grbl/grbl-sim).

## Building

To build the following dependencies are required:
- A working C/C++ compiler
- CMake >= 3.13
- [libgpiod](https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/) (optional)
- CppUnit (optional)

To build the application:
```bash
rm -rf build && mkdir build
cd build && cmake3 .. -DTESTS=ON

make -j4
```

## Testing

To run unit-tests
```bash
./tests/test_all

# Or using CMake:
make test
```

## Gpiod back-end

To configure and active gpios:

### Gpiod library installation
Make sure the **libgpiod** library is installed:
```bash
# on Debian-based Linux distros simply do
apt-get install libgpiod-dev

# on others see https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/about/
```

### Pin mapping
Prepare a configuration file to map logical I/O Grbl pins to physical GPIO lines.

The possible logical I/O pin classes are:
- **EN_\<axis\>**, digital output pin for enabling/disabling stepper motor power;
- **DIR_\<axis\>**, digital output pin for stepper motor direction;
- **STEP_\<axis\>**, digital output pin for step pulse generation.

And **\<axis\>** can be one between `X, Y, Z, A, B, C, U` and `V` according to configured number of axes.

About the physical GPIO lines, they depend on the specific platform.
So, refer to the relevant documentation to find out the available GPIO lines.

An easier way to discover them is to use the **gpioinfo** tool from the package [gpiod](https://packages.debian.org/sid/gpiod):
```bash
# on Debian
apt-get install gpiod

# once installed
gpioinfo
## ...
##  gpiochip0 - 32 lines:
##  	line   0:  "MDIO_DATA"       unused   input  active-high
##  	line   1:   "MDIO_CLK"       unused   input  active-high
##  	line   2:  "SPI0_SCLK"      "P9_22"   input  active-high [used]
##  	line   3:    "SPI0_D0"      "P9_21"   input  active-high [used]
##  	line   4:    "SPI0_D1"      "P9_18"   input  active-high [used]
##  	line   5:   "SPI0_CS0"      "P9_17"   input  active-high [used]
##  	line   6:   "SPI0_CS1"         "cd"   input   active-low [used]
##  	line   7: "ECAP0_IN_PWM0_OUT" "P9_42" input active-high [used]
##    ...
```

An example of _gpio.conf_ file used with a [BeagleBone Black](https://beagleboard.org/black) is:
```
STEP_X:gpiochip2/3
STEP_Y:gpiochip2/4
STEP_Z:gpiochip1/12
DIR_X:gpiochip2/2
DIR_Y:gpiochip2/5
DIR_Z:gpiochip1/13
EN_X:gpiochip0/26
EN_Y:gpiochip1/14
EN_Z:gpiochip2/1
```

### GPIOs enabling

Finally, to use the General-Purpose Input/Output, enable it first via `LIBGPIOD` CMake option:
```bash
cmake .. -DLIBGPIOD=ON
```
Then, run _grbl-gpiod_ passing the config file:
```bash
## From your build-dir
./src/grbl-gpiod --gpio ./gpio.conf
```

## Hardware Triggers (emulation)

Some hardware and software triggers have been implemented as Auxiliary pins, those
can be used to suspend/resume motion:
```bash
## From your build-dir
./src/grbl-gpiod
# GrblHAL 1.1f ['$' or '$HELP' for help]

$PINS
[PIN:0,Aux out 0,Hardware Trigger]
[PIN:1,Aux out 0,Pause/Resume]

## To arm the hardware trigger:
M64P0

## To arm the hardware trigger when motion starts:
M62P0 X10
M62P0 X20

# Soft-Trigger first motion (emulating the hardware trigger)
M65P0
```

**Note:** This plugin is not yet connected to libgpiod.

## Virtual-limits

A virtual-limits back-end can be enabled using `VIRTUAL_LIMITS` CMake option:
```bash
cmake .. -DVIRTUAL_LIMITS=ON
```

Once enabled some virtual-limits pins are available:
```bash
## From your build-dir
./src/grbl-gpiod
# GrblHAL 1.1f ['$' or '$HELP' for help]

$PINS
## ...
# [PIN:2,Aux out 1,X-axis Limit Pin Min]
# [PIN:3,Aux out 1,Y-axis Limit Pin Min]
# [PIN:4,Aux out 1,Z-axis Limit Pin Min]
# [PIN:5,Aux out 1,A-axis Limit Pin Min]
# [PIN:6,Aux out 1,B-axis Limit Pin Min]
# [PIN:7,Aux out 1,C-axis Limit Pin Min]
# [PIN:8,Aux out 1,U-axis Limit Pin Min]
# [PIN:9,Aux out 1,V-axis Limit Pin Min]
# [PIN:10,Aux out 1,X-axis Limit Pin Max]
# [PIN:11,Aux out 1,Y-axis Limit Pin Max]
# [PIN:12,Aux out 1,Z-axis Limit Pin Max]
# [PIN:13,Aux out 1,A-axis Limit Pin Max]
# [PIN:14,Aux out 1,B-axis Limit Pin Max]
# [PIN:15,Aux out 1,C-axis Limit Pin Max]
# [PIN:16,Aux out 1,U-axis Limit Pin Max]
# [PIN:17,Aux out 1,V-axis Limit Pin Max]
# ok

## To activate a limit (ex: X-axis Limit Min):
M64P2

## To de-activate a limit (ex: B-axis Limit Max):
M65P14
```
**Note:** Limits will trigger an Alarm only if the axis is moved towards its
direction.

**Example:**
```bash
## From your build-dir
./src/grbl-gpiod
# GrblHAL 1.1f ['$' or '$HELP' for help]

## activate X-axis Limit Max
M64P8
# ok

## Enable hard-limits
$21=1
# ok

## Send a motion
X10
# ok
# ALARM:1
# [MSG:Reset to continue]

## send `ctrl-x` then `$X` and `ctrl-x` again to reset-unlock-reset grbl
```
